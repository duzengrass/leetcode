package biz.vzero;

/**
 * @author duzeng
 * @className LightSwitch.java
 * @description TODO
 * @date 2022年01月30日 23:39
 */
public class BulbSwitch {
    public int doa(int n) {
        var arr = new boolean[n];

        for (int i = 0; i <n ; i++) {
            arr[i] =true;
        }

        var total = n;
        for (int i = 2; i <= n; i++) {

            var round = n /i;
            for (int j = 1; j <=round; j++) {
                arr[i*j-1] = !arr[i*j-1];
                if (arr[i*j-1]) {
                    total++;
                } else {
                    total--;
                }
            }
        }
        return total;
    }
}
