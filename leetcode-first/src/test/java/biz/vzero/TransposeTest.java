package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TransposeTest {

    static List<Arguments> transpose() {
        return List.of(
                Arguments.arguments(new int[][]{
                        new int[]{1, 2, 3},
                        new int[]{4, 5, 6},
                        new int[]{7, 8, 9}
                }, new int[][]{
                        new int[]{1, 4, 7},
                        new int[]{2, 5, 8},
                        new int[]{3, 6, 9}
                }),
                Arguments.arguments(
                        new int[][]{new int[]{1, 2, 3}, new int[]{4, 5, 6}},
                        new int[][]{new int[]{1, 4}, new int[]{2, 5}, new int[]{3, 6}}
                )
        );
    }

    @ParameterizedTest
    @MethodSource
    void transpose(int[][] matrix, int[][] expected) {
        int[][] actual = new Transpose().transpose(matrix);
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], actual[i]);
        }
    }
}