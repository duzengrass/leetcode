package biz.vzero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestValidParenthesesTest {

    @Test
    void longestValidParentheses() {
        int actual = new LongestValidParentheses().longestValidParentheses("(()");
        int expected = 0;
        assertEquals(expected, actual);
    }
}