package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddTwoNumberTest {

    @ParameterizedTest
    @MethodSource
    public void testAddTwoNumber(ListNode l1, ListNode l2, ListNode expected) {
        ListNode result = new AddTwoNumber().addTwonumber(l1, l2);
        assertEquals(expected, result);
    }

    static List<Arguments> testAddTwoNumber() {
        return Arrays.asList(
                Arguments.arguments(new ListNode(2, new ListNode(4, new ListNode(3))), new ListNode(5, new ListNode(6, new ListNode(4))),
                        new ListNode(7, new ListNode(0, new ListNode(8)))),
                Arguments.arguments(new ListNode(0), new ListNode(0), new ListNode(0)),
                Arguments.arguments(
                        new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9))))))),
                        new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9)))),
                        new ListNode(8, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(0, new ListNode(0, new ListNode(0, new ListNode(1))))))))
                )
        );
    }

}