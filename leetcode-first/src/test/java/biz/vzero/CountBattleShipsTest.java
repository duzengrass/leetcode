package biz.vzero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountBattleShipsTest {

    @Test
    void countBattleships() {
        int expected = 2;
        char[][] board = {
                {'X', '.', '.', 'X'},
                {'.', '.', '.', 'X'},
                {'.', '.', '.', 'X'},
        };
        int actual = new CountBattleShips().countBattleships(board);
        assertEquals(expected, actual);
    }
}