package biz.vzero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfSubstringsTest {

    @Test
    void numberOfSubstrings() {
        int actual = new NumberOfSubstrings().numberOfSubstrings("abcabc");

        assertEquals(10, actual);
    }
}