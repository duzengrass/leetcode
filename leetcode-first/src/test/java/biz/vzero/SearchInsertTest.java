package biz.vzero;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SearchInsertTest {

    @ParameterizedTest
    @MethodSource
    public void testSearchInsert(int[] nums, int target, int expected) {
        int actual = new SearchInsert().searchInsert(nums, target);
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource
    public void testSearchInsertUseHalfInterval(int[] nums, int target, int expected) {
        int actual = new SearchInsert().searchInsertUseHalfInterval(nums, target);
        assertEquals(expected, actual);
    }

    static List<Arguments> testSearchInsert() {
        return Arrays.asList(Arguments.arguments(new int[]{1, 3, 5, 6}, 5, 2),
                Arguments.arguments(new int[]{1, 3, 5, 6}, 2, 1),
                Arguments.arguments(new int[]{1, 3, 5, 6}, 7, 4),
                Arguments.arguments(new int[]{1, 3, 5, 6}, 0, 0)
        );
    }

    static List<Arguments> testSearchInsertUseHalfInterval() {
        return Arrays.asList(Arguments.arguments(new int[]{1, 3, 5, 6}, 5, 2),
                Arguments.arguments(new int[]{1, 3, 5, 6}, 2, 1),
                Arguments.arguments(new int[]{1, 3, 5, 6}, 7, 4),
                Arguments.arguments(new int[]{1, 3, 5, 6}, 0, 0)
        );
    }
}