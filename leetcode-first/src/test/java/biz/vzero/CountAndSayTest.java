package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CountAndSayTest {

    @ParameterizedTest
    @MethodSource
    public void testCountAndSay(int n , String expected) {
        String actual = new CountAndSay().countAndSay(n);
        assertEquals(expected, actual);
    }
    static List<Arguments> testCountAndSay(){
        return Arrays.asList(
                Arguments.arguments(1,"1"),
                Arguments.arguments(4,"1211"),
                Arguments.arguments(5,"111221")
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testCountAndSayUsePointer(int n , String expected) {
        String actual = new CountAndSay().countAndSayUsePointer(n);
        assertEquals(expected, actual);
    }
    static List<Arguments> testCountAndSayUsePointer(){
        return Arrays.asList(
                Arguments.arguments(1,"1"),
                Arguments.arguments(4,"1211"),
                Arguments.arguments(5,"111221")
        );
    }
}