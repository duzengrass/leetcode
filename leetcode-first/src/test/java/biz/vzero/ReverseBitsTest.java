package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReverseBitsTest {

    static List<Arguments> reverseBits() {
        return List.of(
                Arguments.arguments(0b00000010100101000001111010011100, 0b00111001011110000010100101000000),
                Arguments.arguments(0b11111111111111111111111111111101, 0b10111111111111111111111111111111)
        );
    }

    @ParameterizedTest
    @MethodSource
    void reverseBits(int n, int expected) {
        int actual = new ReverseBits().reverseBits(n);
        assertEquals(expected, actual);
    }
    static List<Arguments> reverseBitsUsePartition() {
        return List.of(
                Arguments.arguments(0b00000010100101000001111010011100, 0b00111001011110000010100101000000),
                Arguments.arguments(0b11111111111111111111111111111101, 0b10111111111111111111111111111111)
        );
    }
    @ParameterizedTest
    @MethodSource
    void reverseBitsUsePartition(int n, int expected) {
        int actual = new ReverseBits().reverseBitsUsePartition(n);
        assertEquals(expected, actual);
    }
}