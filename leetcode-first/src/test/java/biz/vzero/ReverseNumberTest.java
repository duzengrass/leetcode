package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.ArgumentUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ReverseNumberTest {

    @ParameterizedTest
    @MethodSource
    public void testReverse(int org , int expected ){
        int actual=new ReverseNumber().reverse(org);
        int actual2=new ReverseNumber().reverseUsePopPush(org);
        assertEquals(expected,actual);
        assertEquals(expected,actual2);
    }

    static List<Arguments> testReverse(){
        return Stream.of(
                Arguments.arguments(123,321),
                Arguments.arguments(-123,-321),
                Arguments.arguments(120,21),
                Arguments.arguments(0,0)
        ).collect(Collectors.toList());
    }
}