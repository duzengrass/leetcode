package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class NumbersOfBinaryTest {
    static List<Arguments> hammingWeight() {
        return List.of(
                Arguments.arguments(0b00000000000000000000000000001011, 3),
                Arguments.arguments(0b00000000000000000000000010000000, 1),
                Arguments.arguments(0b11111111111111111111111111111101, 31)
        );
    }

    @ParameterizedTest
    @MethodSource
    void hammingWeight(int n, int expected) {
        int actual = new NumbersOfBinary().hammingWeight(n);
        assertEquals(expected, actual);
    }

    static List<Arguments> hammingWeightUseNN1() {
        return List.of(
                Arguments.arguments(0b00000000000000000000000000001011, 3),
                Arguments.arguments(0b00000000000000000000000010000000, 1),
                Arguments.arguments(0b11111111111111111111111111111101, 31)
        );
    }

    @ParameterizedTest
    @MethodSource
    void hammingWeightUseNN1(int n, int expected) {
        int actual = new NumbersOfBinary().hammingWeightUseNN1(n);
        assertEquals(expected, actual);
    }

    static List<Arguments> hammingWeightUseJdk() {
        return List.of(
                Arguments.arguments(0b00000000000000000000000000001011, 3),
                Arguments.arguments(0b00000000000000000000000010000000, 1),
                Arguments.arguments(0b11111111111111111111111111111101, 31)
        );
    }

    @ParameterizedTest
    @MethodSource
    void hammingWeightUseJdk(int n, int expected) {
        int actual = new NumbersOfBinary().hammingWeightUseJdk(n);
        assertEquals(expected, actual);
    }
}