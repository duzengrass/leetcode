package biz.vzero;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SumOfTwoTest {

    @ParameterizedTest
    @MethodSource
    public void testTwoSum(int[] nums, int target, int[] expected) {
        int[] actual = new SumOfTwo().twoSum(nums, target);
        assertArrayEquals(expected, actual);


    }

    static List<Arguments> testTwoSum() {
        return Stream.of(
                Arguments.arguments(new int[]{2, 7, 11, 15}, 9, new int[]{0, 1}),
                Arguments.arguments(new int[]{3, 2, 4}, 6, new int[]{1, 2}),
                Arguments.arguments(new int[]{3, 3}, 6, new int[]{0, 1})
        ).collect(Collectors.toList());
    }

    @ParameterizedTest
    @MethodSource
    public void testTwoSumUseMap(int[] nums, int target ,int [] expected) {
        int[] actual = new SumOfTwo().twoSum(nums, target);
        assertArrayEquals(expected, actual);
    }
    static List<Arguments> testTwoSumUseMap(){
        return Stream.of(
                Arguments.arguments(new int[]{2, 7, 11, 15}, 9, new int[]{0, 1}),
                Arguments.arguments(new int[]{3, 2, 4}, 6, new int[]{1, 2}),
                Arguments.arguments(new int[]{3, 3}, 6, new int[]{0, 1})
        ).collect(Collectors.toList());
    }
}