package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CheckStraightLineTest {

    @ParameterizedTest
    @MethodSource
    public void testCheckStraightLine(int[][] coordinates, boolean expected) {
        boolean actual = new CheckStraightLine().checkStraightLine(coordinates);
        assertEquals(expected, actual);
    }

    static List<Arguments> testCheckStraightLine() {
        return Arrays.asList(
                Arguments.arguments(new int[][]{{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 6}, {7, 7}}, false)
                , Arguments.arguments(new int[][]{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7}}, true)
                , Arguments.arguments(new int[][]{{-3, -2}, {-1, -2}, {2, -2}, {-2, -2}, {0, -2}}, true)
                , Arguments.arguments(new int[][]{{0, 0}, {0, 5}, {5, 5}, {5, 0}}, false)
        );
    }


    @ParameterizedTest
    @MethodSource
    public void testCheckStraightLineUseDeterminant(int[][] coordinates, boolean expected) {
        boolean actual = new CheckStraightLine().checkStraightLineUseDeterminant(coordinates);
        assertEquals(expected, actual);
    }

    static List<Arguments> testCheckStraightLineUseDeterminant() {
        return Arrays.asList(
                Arguments.arguments(new int[][]{{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 6}, {7, 7}}, false)
                , Arguments.arguments(new int[][]{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7}}, true)
                , Arguments.arguments(new int[][]{{-3, -2}, {-1, -2}, {2, -2}, {-2, -2}, {0, -2}}, true)
                , Arguments.arguments(new int[][]{{0, 0}, {0, 5}, {5, 5}, {5, 0}}, false)
        );
    }
}