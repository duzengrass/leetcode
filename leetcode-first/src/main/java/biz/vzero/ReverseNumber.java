package biz.vzero;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReverseNumber {
    public int reverse(int x) {
        List<Integer> result = new ArrayList<>();
        long temp = x;
        long sum = 0;
        while (temp != 0) {
            int reminder = (int) (temp % 10);
            sum = sum * 10 + reminder;
            temp = temp / 10;
        }
        if (sum >= Integer.MIN_VALUE && sum <= Integer.MAX_VALUE) {
            return (int) sum;
        }
        return 0;
    }

    //(map (+) ("foo"))
    public int reverseUsePopPush(int x) {

        int result = 0;
        while (x != 0) {
            int pop = x % 10;
            x /= 10;
            if (result > Integer.MAX_VALUE / 10 || (result == Integer.MAX_VALUE / 10 && pop > 7)) return 0;
            if (result < Integer.MIN_VALUE / 10 || (result == Integer.MIN_VALUE / 10 && pop < -8)) return 0;
            result = result * 10 + pop;
        }

        return result;

    }
}
