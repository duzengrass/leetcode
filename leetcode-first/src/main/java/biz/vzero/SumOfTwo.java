package biz.vzero;

import java.util.HashMap;
import java.util.Map;

public class SumOfTwo {
    public int[] twoSum(int[] nums, int target) {
        if (nums.length < 2) throw new IllegalArgumentException("数组个数不能小于2");
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if ((nums[i] + nums[j]) == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[0];
    }

    public int[] twoSumUseMap(int[] nums, int taget) {
        Map<Integer, Integer> table = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (table.containsKey(taget - nums[i])) {
                return new int[]{table.get(taget - nums[i]), i};
            }
            table.put(nums[i], i);


        }
        return new int[0];
    }


}
