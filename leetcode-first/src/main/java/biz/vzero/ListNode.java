package biz.vzero;

import java.util.Objects;

public class ListNode {

    int val;

    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        if (val>9 || val<0 ) throw new IllegalArgumentException("val应为个位数");
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public int length() {
        if (next == null) return 1;
        return next.length() + 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListNode listNode = (ListNode) o;
        return val == listNode.val && Objects.equals(next, listNode.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val, next);
    }
}
