package biz.vzero;

public class NumbersOfBinary {
    public int hammingWeight(int n) {
        int count = 0;
        while (n != 0) {
            count += n & 1;
            n >>>= 1;

        }
        return count;
    }

    public int hammingWeightUseNN1(int n) {
        int res = 0;
        while (n != 0) {
            res++;
            n &= n - 1;
        }
        return res;
    }

    public int hammingWeightUseJdk(int n) {
        return Integer.bitCount(n);
    }
}
