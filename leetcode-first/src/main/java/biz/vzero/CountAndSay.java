package biz.vzero;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CountAndSay {
    public String countAndSay(int n) {
        if (n == 1) return "1";
        Integer[] arr = Arrays.stream(countAndSay(n - 1).split("")).map(item -> Integer.valueOf(item)).toArray(Integer[]::new);
        int last = 0, lastSum = 0;
        String result = "";
        for (Integer item : arr
        ) {
            if (item.equals(last)) {
                lastSum += 1;
                continue;
            }
            if (last != 0) {
                result += String.format("%s%s", lastSum, last);
            }
            lastSum = 1;
            last = item;
        }
        result += String.format("%s%s", lastSum, last);
        return result;
    }

    public String countAndSayUsePointer(int n) {
        if (n == 1) return "1";
        String lastString = countAndSay(n - 1);
        int start = 0, end = 0;
        StringBuilder result = new StringBuilder();
        while (end < lastString.length()) {
            if (lastString.charAt(end) != lastString.charAt(start)) {
                result.append(end - start).append(lastString.charAt(start));
                start = end;
            }
            end++;
        }
        result.append(end - start).append(lastString.charAt(start));
        return result.toString();
    }
}
