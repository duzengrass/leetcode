package biz.vzero;

public class CheckStraightLine {
    //δx/δy方式求解，涉及到除数为0的判断，另外垂直于X、Y轴的也要判断
    public boolean checkStraightLine(int[][] coordinates) {
        if (coordinates.length == 2) return true;
        int index = 0;
        while (index < coordinates.length - 2) {
            int[] point0 = coordinates[index], point1 = coordinates[index + 1], point2 = coordinates[index + 2];

            double slope1 = point0[1] - point1[1] == 0 ? Double.POSITIVE_INFINITY : (point0[0] - point1[0]) * 1.0 / (point0[1] - point1[1]);
            double slope2 = point1[1] - point2[1] == 0 ? Double.POSITIVE_INFINITY : (point1[0] - point2[0]) * 1.0 / (point1[1] - point2[1]);
            if (slope1 != slope2) return false;
            index++;
        }

        return true;
    }

    /**
     * 线性代数，|a,b|=0
     */
    public boolean checkStraightLineUseDeterminant(int[][] coordinates) {
        if (coordinates.length == 2) return true;

        int deltaX1=coordinates[1][0]-coordinates[0][0];
        int deltaY1=coordinates[1][1]-coordinates[0][1];
        for (int i = 2; i < coordinates.length; i++) {
            int deltaXi=coordinates[i][0]-coordinates[0][0];
            int deltaYi=coordinates[i][1]-coordinates[0][1];

            if (!(deltaX1*deltaYi==deltaXi*deltaY1)) return false;
        }
        return true;
    }
}
