package biz.vzero.p6.p6;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * @author duzeng
 * @className PersonGenerator.java
 * @description TODO
 * @date 2024年08月13日 22:15
 */
public class PersonGenerator {
    public static List<Person> generatePersonList(int size) {
        var ret = new ArrayList<Person>();
        var firstNames = new String[] { "Mary", "Patricia", "Linda", "Barbara", "Elizabeth", "James", "John", "Robert",
                "Michael", "William" };
        var lastNames = new String[] { "Smith", "Jones", "Taylor", "Brown", "Williams", "Johnson", "Lee", "Green",
                "Davies", "Evans" };

        var randomGenerator = new Random();

        for (int i = 0; i < size; i++) {
            var person = new Person();
            person.setId(i);
            person.setFirstName(firstNames[randomGenerator.nextInt(firstNames.length)]);
            person.setLastName(lastNames[randomGenerator.nextInt(lastNames.length)]);
            person.setSalary(randomGenerator.nextInt(100000));
            person.setCoeficient(randomGenerator.nextDouble() * 10);

            var calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, -randomGenerator.nextInt(30));
            var birthDate = calendar.getTime();
            person.setBirthDate(birthDate);
            ret.add(person);
        }

        return ret;
    }
}
