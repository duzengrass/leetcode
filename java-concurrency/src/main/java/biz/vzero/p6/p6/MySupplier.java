package biz.vzero.p6.p6;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * @author duzeng
 * @className MySupplier.java
 * @description TODO
 * @date 2024年08月13日 22:21
 */
public class MySupplier implements Supplier<String> {
    private final AtomicInteger counter;

    public MySupplier() {
        this.counter = new AtomicInteger(0);
    }


    @Override
    public String get() {
        return "String " + counter.getAndIncrement();
    }
}
