package biz.vzero.p6.p6;

import java.util.Date;

/**
 * @author duzeng
 * @className Person.java
 * @description TODO
 * @date 2024年08月13日 22:12
 */
public class Person implements Comparable<Person> {
   private int id;
   private String firstName;
    private String lastName;

    private Date birthDate;
    private int salary;
    private double coeficient;


    public int compareTo(Person o) {
        int compareLastNames = this.lastName.compareTo(o.lastName);
        if (compareLastNames != 0) {
            return compareLastNames;
        } else {
            return this.firstName.compareTo(o.firstName);
        }
    }

    public int getId() {
        return id;
    }

    public Person setId(int id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Person setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Person setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Person setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public int getSalary() {
        return salary;
    }

    public Person setSalary(int salary) {
        this.salary = salary;
        return this;
    }

    public double getCoeficient() {
        return coeficient;
    }

    public Person setCoeficient(double coeficient) {
        this.coeficient = coeficient;
        return this;
    }
}
