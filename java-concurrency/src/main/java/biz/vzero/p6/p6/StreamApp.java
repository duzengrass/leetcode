package biz.vzero.p6.p6;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

/**
 * @author duzeng
 * @className StreamApp.java
 * @description TODO
 * @date 2024年08月13日 22:22
 */
public class StreamApp {
    private static final Logger log = LoggerFactory.getLogger(StreamApp.class);

    public static void main(String[] args) {
        // parallelStream
        log.info("From a Collection: ");
        var persons = PersonGenerator.generatePersonList(10000);
        var parallelStream = persons.parallelStream();
        log.info("Number of persons: {}", parallelStream.count());

        log.info("**********************************************************");

        //Stream.generate
        log.info("From a Supplier: ");
        var mySupplier = new MySupplier();
        var stream = Stream.generate(mySupplier);
        stream.limit(10).forEach(s -> log.info("String: {}", s));

        log.info("**********************************************************");
        //Stream.of
        log.info("From a predefined set of elements: ");
        var elementsStream = Stream.of("Peter", "John", "Mary");
        elementsStream.parallel().forEach(log::info);

        log.info("**********************************************************");
        //BufferReader.lines
        log.info("From a file: ");
        try (var bufferedReader = new BufferedReader(new FileReader("data/data.txt"))) {
            var lines = bufferedReader.lines();
            log.info("Number of lines: {}", lines.parallel().count());
            log.info("************************");
        }  catch (java.io.IOException e) {
            log.error("Error", e);
        }

        log.info("**********************************************************");

        //Files.list
        log.info("From a directory: ");
        try {
            var directoryContent = Files.list(Paths.get("."));
//            log.info("Number of files: {}", directoryContent.parallel().count());
            directoryContent.parallel().forEach(item-> log.info(item.getFileName().toString()));
            log.info("************************");
        } catch (java.io.IOException e) {
            log.error("Error", e);
        }

        log.info("**********************************************************");

        // Arrays.stream
        log.info("From an array: ");
        var array = new String[] { "1", "2", "3", "4", "5" };
        var streamFromArray= Arrays.stream(array);
        streamFromArray.parallel().forEach(log::info);

        log.info("**********************************************************");

        //DoubleStream
        log.info("From a DoubleStream: ");
        var random = new Random();
        var doubleStream = random.doubles(10);
        var doubleStreamAverage = doubleStream.parallel().peek(d -> log.info("Value: {}", d)).average().getAsDouble();


        log.info("**********************************************************");

        //Stream.concat
        log.info("Concatenating two streams: ");
        var stream1 = Stream.of("1", "2", "3");
        var stream2 = Stream.of("4", "5", "6");
        var concatStream = Stream.concat(stream1, stream2);
        concatStream.parallel().forEach(log::info);

        log.info("**********************************************************");
    }
}
