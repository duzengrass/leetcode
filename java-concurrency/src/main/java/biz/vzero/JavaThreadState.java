package biz.vzero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class JavaThreadState {

    private static final Logger logger = LoggerFactory.getLogger(JavaThreadState.class);

    private static final Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        var thread1 = new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            synchronized (lock) {
                logger.info("Hello world!");
            }
            logger.info("Thread state inside: {}", Thread.currentThread().getState());
        });
        var thread2 = new Thread(() -> {
            synchronized (lock) {
                logger.info("Hello world!");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            logger.info("Thread state inside: {}", Thread.currentThread().getState());
        });
        var thread3 = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                logger.info("Thread state  {}", thread1.getState());
            }
        });
        thread3.start();
        logger.info("Thread state outside: {}", thread1.getState());
        thread1.start();
        thread2.start();
        logger.info("Thread state outside: {}", thread1.getState());
        thread1.join();
        logger.info("Thread state outside: {}", thread1.getState());
    }
}
