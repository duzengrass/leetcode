package biz.vzero.p5.p5_2;

/**
 * @author duzeng
 * @className Product.java
 * @description TODO
 * @date 2024年08月11日 23:38
 */
public class Product {
    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Product setPrice(double price) {
        this.price = price;
        return this;
    }
}
