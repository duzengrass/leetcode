package biz.vzero.p5.p5_2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author duzeng
 * @className ProductListGenerator.java
 * @description TODO
 * @date 2024年08月11日 23:39
 */
public class ProductListGenerator {

    public List<Product> generate(int size) {
        var ret = new ArrayList<Product>();
        for (int i = 0; i < size; i++) {
            var product = new Product();
            product.setName("Product " + i);
            product.setPrice(10);
            ret.add(product);
        }
        return ret;
    }
}
