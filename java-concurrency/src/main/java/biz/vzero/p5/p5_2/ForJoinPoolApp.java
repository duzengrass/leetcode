package biz.vzero.p5.p5_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ForkJoinPool;

/**
 * @author duzeng
 * @className ForJoinPoolApp.java
 * @description TODO
 * @date 2024年08月11日 23:13
 */
public class ForJoinPoolApp {
    private static final Logger log = LoggerFactory.getLogger(ForJoinPoolApp.class);

    public static void main(String[] args) {

        var generator = new ProductListGenerator();
        var products = generator.generate(10000);

        var task = new Task(products, 0, products.size(), 0.20);

        var pool = new ForkJoinPool();

        pool.execute(task);


        do {
            log.info("Main: Thread Count: {}", pool.getActiveThreadCount());
            log.info("Main: Thread Steal: {}", pool.getStealCount());
            log.info("Main: Parallelism: {}", pool.getParallelism());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error("Main: Error", e);
            }
        } while (!task.isDone());


        pool.shutdown();

        if (task.isCompletedNormally()) {
            log.info("Main: The process has completed normally.");
        }

        for (var product : products) {
            if (product.getPrice() != 12) {
                log.info("Product {} : {}", product.getName(), product.getPrice());
            }
        }

        log.info("Main: End of the program.");
    }
}
