package biz.vzero.p3.p3_3;

import java.util.concurrent.TimeUnit;

/**
 * @author duzeng
 * @className Participant.java
 * @description TODO
 * @date 2024年08月04日 20:10
 */
public class Participant implements Runnable {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Participant.class);
    private VideoConference conference;
    private String name;

    public Participant(VideoConference conference, String name) {
        this.conference = conference;
        this.name = name;
    }

    @Override
    public void run() {
        long duration = (long) (Math.random() * 10);
        try {
            TimeUnit.SECONDS.sleep(duration);
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        }
        conference.arrive(name);
    }
}
