package biz.vzero.p3.p3_3;

/**
 * @author duzeng
 * @className CountDownLatchApp.java
 * @description TODO
 * @date 2024年08月04日 20:12
 */
public class CountDownLatchApp {
    public static void main(String[] args) {
        var videoConference = new VideoConference(10);
        var thread = new Thread(videoConference);
        thread.start();

        for (var i = 0; i < 10; i++) {
            var participant = new Participant(videoConference, "Participant-" + (i + 1));
            var participantThread = new Thread(participant);
            participantThread.start();
        }
    }
}
