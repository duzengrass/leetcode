package biz.vzero.p3.p3_3;

import java.util.concurrent.CountDownLatch;

/**
 * @author duzeng
 * @className VideoConference.java
 * @description TODO
 * @date 2024年08月04日 20:08
 */
public class VideoConference  implements Runnable{

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(VideoConference.class);

    private final CountDownLatch controller;

    public VideoConference(int number) {
        controller = new CountDownLatch(number);
    }

    public void arrive(String name) {
        log.info("{} has arrived.", name);
        controller.countDown();
        log.info("VideoConference: Waiting for other {} participants.", controller.getCount());
    }

    @Override
    public void run() {
        log.info("VideoConference: Initialization: {} participants.", controller.getCount());
        try {
            controller.await();
            log.info("VideoConference: All the participants have come");
            log.info("VideoConference: Let's start...");
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        }
    }
}
