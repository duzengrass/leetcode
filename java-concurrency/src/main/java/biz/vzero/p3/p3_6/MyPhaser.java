package biz.vzero.p3.p3_6;

import java.util.concurrent.Phaser;

/**
 * @author duzeng
 * @className MyPhaser.java
 * @description TODO
 * @date 2024年08月08日 20:59
 */
public class MyPhaser extends Phaser {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(MyPhaser.class);
    @Override
    protected boolean onAdvance(int phase, int registeredParties) {
        switch (phase) {
            case 0:
                return studentsArrived();
            case 1:
                return finishFirstExercise();
            case 2:
                return finishSecondExercise();
            case 3:
                return finishExam();
            default:
                return true;
        }
    }

    private boolean studentsArrived() {
        log.info("Phaser: The exam are going to start. The students are ready.");
        log.info("Phaser: We have {} students.", getRegisteredParties());
        return false;
    }

    private boolean finishFirstExercise() {
        log.info("Phaser: All the students have finished the first exercise.");
        log.info("Phaser: It's time for the second one.");
        return false;
    }

    private boolean finishSecondExercise() {
        log.info("Phaser: All the students have finished the second exercise.");
        log.info("Phaser: It's time for the third one.");
        return false;
    }

    private boolean finishExam() {
        log.info("Phaser: All the students have finished the exam.");
        log.info("Phaser: Thank you for your time.");
        return true;
    }

}
