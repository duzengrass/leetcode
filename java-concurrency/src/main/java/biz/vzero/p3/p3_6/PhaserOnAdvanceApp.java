package biz.vzero.p3.p3_6;

/**
 * @author duzeng
 * @className PhaserOnAdvanceApp.java
 * @description TODO
 * @date 2024年08月08日 21:05
 */
public class PhaserOnAdvanceApp {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PhaserOnAdvanceApp.class);

    public static void main(String[] args) {
        var phaser = new MyPhaser();

        var students = new Student[5];

        for (var i = 0; i < students.length; i++) {
            students[i] = new Student(phaser);
            phaser.register();
        }

        var threads = new Thread[students.length];
        for (int i = 0; i < students.length; i++) {
            threads[i] = new Thread(students[i], "Student " + i);
            threads[i].start();
        }

        for (var i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }

        log.info("Main: The phaser has finished: {}", phaser.isTerminated());
    }
}
