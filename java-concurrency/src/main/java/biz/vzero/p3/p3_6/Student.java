package biz.vzero.p3.p3_6;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * @author duzeng
 * @className Student.java
 * @description TODO
 * @date 2024年08月08日 21:01
 */
public class Student implements Runnable{
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Student.class);

    private Phaser phaser;

    public Student(Phaser phaser) {
        this.phaser = phaser;
    }

    @Override
    public void run() {
        log.info("{}: Has arrived.", Thread.currentThread().getName());
        phaser.arriveAndAwaitAdvance();
        log.info("{}: Is going to do the first exercise..", Thread.currentThread().getName());
        doExercise1();
        log.info("{}: Has done the first exercise.", Thread.currentThread().getName());
        phaser.arriveAndAwaitAdvance();
        log.info("{}: Is going to do the second exercise..", Thread.currentThread().getName());
        doExercise2();
        log.info("{}: Has done the second exercise.", Thread.currentThread().getName());
        phaser.arriveAndAwaitAdvance();
        log.info("{}: Is going to do the third exercise..", Thread.currentThread().getName());
        doExercise3();
        log.info("{}: Has finished the exam.", Thread.currentThread().getName());
        phaser.arriveAndAwaitAdvance();

      }


        private void doExercise1() {
            long duration = (long) (Math.random() * 10);
            try {
                TimeUnit.SECONDS.sleep(duration);
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }


        private void doExercise2() {
            long duration = (long) (Math.random() * 10);
            try {
                TimeUnit.SECONDS.sleep(duration);
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }

        private void doExercise3() {
            long duration = (long) (Math.random() * 10);
            try {
                TimeUnit.SECONDS.sleep(duration);
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }
}
