package biz.vzero.p3.p3_5;

/**
 * @author duzeng
 * @className PhaserApp.java
 * @description TODO
 * @date 2024年08月05日 23:40
 */
public class PhaserApp {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PhaserApp.class);

    public static void main(String[] args) {
        var phaser = new java.util.concurrent.Phaser(3);
        var fileSearch1 = new FileSearch("C:\\Windows", "log", phaser);
        var fileSearch2 = new FileSearch("C:\\Program Files", "log", phaser);
        var fileSearch3 = new FileSearch("C:\\Users\\duzeng\\documents", "log", phaser);

        var thread1 = new Thread(fileSearch1, "System");
        var thread2 = new Thread(fileSearch2, "Apps");
        var thread3 = new Thread(fileSearch3, "Documents");

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        }

        log.info("Terminated: {}", phaser.isTerminated());
    }
}
