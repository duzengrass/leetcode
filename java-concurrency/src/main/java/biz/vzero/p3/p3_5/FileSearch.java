package biz.vzero.p3.p3_5;

import org.slf4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * @author duzeng
 * @className FileSearch.java
 * @description TODO
 * @date 2024年08月05日 23:22
 */
public class FileSearch implements Runnable {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(FileSearch.class);

    private final String initPath;
    private final String fileExtension;

    private List<String> results;

    private Phaser phaser;

    public FileSearch(String initPath, String fileExtension, Phaser phaser) {
        this.initPath = initPath;
        this.fileExtension = fileExtension;
        this.phaser = phaser;
        results = new ArrayList<>();
    }

    private void directoryProcess(File file) {
        File[] list = file.listFiles();
        if (list != null) {
            for (var i = 0; i < list.length; i++) {
                if (list[i].isDirectory()) {
                    directoryProcess(list[i]);
                } else {
                    fileProcess(list[i]);
                }
            }
        }
    }

    private void fileProcess(File file) {
        if (file.getName().endsWith(fileExtension)) {
            results.add(file.getAbsolutePath());
        }
    }

    private void filterResults() {
        var newResults = new ArrayList<String>();
        var actualDate = new Date().getTime();
        for (var i = 0; i < results.size(); i++) {
            File file = new File(results.get(i));
            var fileDate = file.lastModified();
            if (actualDate - fileDate < TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)) {
                newResults.add(results.get(i));
            }
        }
        results = newResults;
    }

    private boolean checkResults() {
        if (results.isEmpty()) {
            log.info("{}: Phase {}: 0 results.", Thread.currentThread().getName(), phaser.getPhase());
            log.info("{}: Phase {}: End.", Thread.currentThread().getName(), phaser.getPhase());
            phaser.arriveAndDeregister();
            return false;
        } else {
            log.info("{}: Phase {}: {} results.", Thread.currentThread().getName(), phaser.getPhase(), results.size());
            phaser.arriveAndAwaitAdvance();
            return true;
        }
    }

    private void showInfo() {
        for (var i = 0; i < results.size(); i++) {
            File file = new File(results.get(i));
            log.info("{}: {}", Thread.currentThread().getName(), file.getAbsolutePath());
        }
        phaser.arriveAndAwaitAdvance();
    }
    @Override
    public void run() {
        phaser.arriveAndAwaitAdvance();
        log.info("{}: Starting.", Thread.currentThread().getName());
        File file = new File(initPath);
        if (file.isDirectory()) {
            directoryProcess(file);
        }
        if (!checkResults()) {
            return;
        }
        filterResults();
        if (!checkResults()) {
            return;
        }
        showInfo();
        phaser.arriveAndDeregister();
        log.info("{}: Work completed.", Thread.currentThread().getName());
    }

}
