package biz.vzero.p3.p3_4;

/**
 * @author duzeng
 * @className Grouper.java
 * @description TODO
 * @date 2024年08月04日 21:40
 */
public class Grouper implements Runnable {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Grouper.class);
    private final Results results;

    public Grouper(Results results) {
        this.results = results;
    }

    @Override
    public void run() {
        var finalResult = 0;
        log.info("Grouper: Processing results...\n");
        var data = results.getData();
        for (var number : data) {
            finalResult += number;
        }
        System.out.printf("Grouper: Total result: %d.\n", finalResult);
    }
}
