package biz.vzero.p3.p3_4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author duzeng
 * @className Searcher.java
 * @description TODO
 * @date 2024年08月04日 21:37
 */
public class Searcher implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(Searcher.class);

    private final int firstRow;
    private final int lastRow;
    private final MatrixMock mock;
    private final Results results;
    private final int number;

    private final CyclicBarrier barrier;

    public Searcher(int firstRow, int lastRow, MatrixMock mock, Results results, int number, CyclicBarrier barrier) {
        this.firstRow = firstRow;
        this.lastRow = lastRow;
        this.mock = mock;
        this.results = results;
        this.number = number;
        this.barrier = barrier;
    }

    @Override
    public void run() {
        log.info("{}: Processing lines from {} to {}.", Thread.currentThread().getName(), firstRow, lastRow);
        for (var i = firstRow; i < lastRow; i++) {
            int[] row = mock.getRow(i);
            int counter = 0;
            for (var j = 0; j < row.length; j++) {
                if (row[j] == number) {
                    counter++;
                }
            }
            results.setData(i, counter);
        }
        log.info("{}: Lines processed.", Thread.currentThread().getName());
        try {
            barrier.await();
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        } catch (BrokenBarrierException e) {
            log.error("BrokenBarrier", e);
        }
        log.info("{}: The barrier has been end.", Thread.currentThread().getName());
    }
}
