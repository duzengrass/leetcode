package biz.vzero.p3.p3_4;

/**
 * @author duzeng
 * @className Results.java
 * @description TODO
 * @date 2024年08月04日 21:02
 */
public class Results {
    private final int[] data;

    public Results(int size) {
        data = new int[size];
    }

    public void setData(int position, int value) {
        data[position] = value;
    }


    public int[] getData() {
        return data;
    }
}
