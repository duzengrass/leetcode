package biz.vzero.p3.p3_4;

/**
 * @author duzeng
 * @className CyclicBarrierApp.java
 * @description TODO
 * @date 2024年08月04日 21:42
 */
public class CyclicBarrierApp {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CyclicBarrierApp.class);

    public static void main(String[] args) {
        final int ROWS = 10000;
        final int NUMBERS = 1000;
        final int SEARCH = 5;
        final int PARTICIPANTS = 5;
        final int LINES_PARTICIPANT = ROWS / PARTICIPANTS;

        var mock = new MatrixMock(ROWS, NUMBERS, SEARCH);

        var results = new Results(ROWS);

        var grouper = new Grouper(results);

        var barrier = new java.util.concurrent.CyclicBarrier(PARTICIPANTS, grouper);

        var searchers = new Searcher[PARTICIPANTS];

        for (var i = 0; i < PARTICIPANTS; i++) {
            var firstRow = i * LINES_PARTICIPANT;
            var lastRow = (i * LINES_PARTICIPANT) + LINES_PARTICIPANT;
            searchers[i] = new Searcher(firstRow, lastRow, mock, results, SEARCH, barrier);
            var thread = new Thread(searchers[i]);
            thread.start();
        }

        log.info("Main: The main thread has finished.");
    }
}
