package biz.vzero.p3.p3_4;

import java.util.Random;

/**
 * @author duzeng
 * @className MatrixMock.java
 * @description TODO
 * @date 2024年08月04日 20:51
 */
public class MatrixMock {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(MatrixMock.class);
    private final int[][] data;

    public MatrixMock(int size, int length, int number) {
        int counter = 0;
        data = new int[size][length];
        var random = new Random();
        for (var i = 0; i < size; i++) {
            for (var j = 0; j < length; j++) {
                data[i][j] = random.nextInt(10);
                if (data[i][j] == number) {
                    counter++;
                }
            }
        }
        log.info("Mock: There are {} occurrences of number[{}] in generated data.", counter, number);
    }

    public int[] getRow(int row) {
        if ((row >= 0) && (row < data.length)) {
            return data[row];
        }
        return null;
    }
}
