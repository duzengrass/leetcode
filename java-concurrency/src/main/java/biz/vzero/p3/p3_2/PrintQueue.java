package biz.vzero.p3.p3_2;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author duzeng
 * @className PrintQueue.java
 * @description TODO
 * @date 2024年08月04日 17:48
 */
public class PrintQueue {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PrintQueue.class);

    private final Semaphore semaphore;
    private final boolean[] freePrinters;
    private final Lock lockPrinters;

    public PrintQueue() {
        semaphore = new Semaphore(3);
        freePrinters = new boolean[3];
        for (var i = 0; i < 3; i++) {
            freePrinters[i] = true;
        }
        lockPrinters = new ReentrantLock();
    }


    public void printJob(Object document) {
        try {
            semaphore.acquire();
            var assignedPrinter = getPrinter();
            var duration = (long) (Math.random() * 10);
            log.info("{}: PrintQueue: Printing a Job in Printer {} during {} seconds\n",
                    Thread.currentThread().getName(), assignedPrinter, duration);
            TimeUnit.SECONDS.sleep(duration);
            freePrinters[assignedPrinter] = true;
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        } finally {
            semaphore.release();
        }
    }

    private int getPrinter() {
        var ret = -1;
        try {
            lockPrinters.lock();
            for (var i = 0; i < freePrinters.length; i++) {
                if (freePrinters[i]) {
                    ret = i;
                    freePrinters[i] = false;
                    break;
                }
            }
        } finally {
            lockPrinters.unlock();
        }
        return ret;
    }
}
