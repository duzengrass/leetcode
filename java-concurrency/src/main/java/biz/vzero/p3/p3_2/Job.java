package biz.vzero.p3.p3_2;

/**
 * @author duzeng
 * @className Job.java
 * @description TODO
 * @date 2024年08月04日 17:52
 */
public class Job implements Runnable{
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Job.class);


    private PrintQueue printQueue;

    public Job(PrintQueue printQueue) {
        this.printQueue = printQueue;
    }

    @Override
    public void run() {
        log.info("{}: Going to print a document", Thread.currentThread().getName());
        printQueue.printJob(new Object());
        log.info("{}: The document has been printed", Thread.currentThread().getName());
    }
}
