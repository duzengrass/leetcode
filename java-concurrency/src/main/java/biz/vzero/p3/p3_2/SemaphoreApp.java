package biz.vzero.p3.p3_2;

/**
 * @author duzeng
 * @className SamphoreApp.java
 * @description TODO
 * @date 2024年08月04日 17:54
 */
public class SemaphoreApp {
    public static void main(String[] args) {
        var printQueue = new PrintQueue();
        var threads = new Thread[12];
        for (var i = 0; i < 12; i++) {
            threads[i] = new Thread(new Job(printQueue), "Thread " + i);
        }

        for (var i = 0; i < 12; i++) {
            threads[i].start();
        }
    }
}
