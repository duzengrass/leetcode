package biz.vzero.p3.p3_8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author duzeng
 * @className SeedGenerator.java
 * @description TODO
 * @date 2024年08月10日 12:44
 */
public class SeedGenerator implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(SeedGenerator.class);

    private CompletableFuture<Integer> resultCommunicator;

    public SeedGenerator(CompletableFuture<Integer> resultCommunicator) {
        this.resultCommunicator = resultCommunicator;
    }

    @Override
    public void run() {
        log.info("SeedGenerator: Generating seed...");
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            log.error("SeedGenerator: Interrupted while generating seed", e);
        }
        int seed = (int) Math.rint(Math.random() * 100);
        log.info("SeedGenerator: Seed generated: {}", seed);
        resultCommunicator.complete(seed);
    }
}
