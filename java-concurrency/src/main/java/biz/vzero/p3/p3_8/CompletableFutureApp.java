package biz.vzero.p3.p3_8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author duzeng
 * @className CompletableFutureApp.java
 * @description TODO
 * @date 2024年08月10日 13:01
 */
public class CompletableFutureApp {
    private static final Logger log = LoggerFactory.getLogger(CompletableFutureApp.class);

    public static void main(String[] args) {
        log.info("Main: Start");
        var seedFuture = new CompletableFuture<Integer>();
        var seedThread = new Thread(new SeedGenerator(seedFuture));
        seedThread.start();

        log.info("Main: Getting the seed...");
        int seed = 0;
        try {
            seed = seedFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("Main: Error getting the seed", e);
        }
        log.info("Main: The seed is: {}", seed);

        log.info("Main: Launching the number list generator...");
        var numberListGenerator = new NumberListGenerator(seed);

        var startFuture = CompletableFuture.supplyAsync(numberListGenerator);

        log.info("Main: Launching the step 1");
        var step1Future = startFuture.thenApplyAsync(list -> {
            log.info("{}: Step 1: Start", Thread.currentThread().getName());
            long selected = 0;
            long selectedDistance = Long.MAX_VALUE;
            long distance;
            for (long number : list) {
                distance = Math.abs(number - 1000);
                if (distance < selectedDistance) {
                    selected = number;
                    selectedDistance = distance;
                }
            }

            log.info("{}: Step 1: Result - {}", Thread.currentThread().getName(), selected);
            return selected;
        });

        log.info("Main: Launching the step 2");
        var step2Future = startFuture.thenApplyAsync(list -> list.stream().max(Long::compareTo).get());

        var write2Future = step2Future.thenAcceptAsync(
                selected -> log.info("{}: Step 2: Result - {}", Thread.currentThread().getName(), selected));

        log.info("Main: Launching the step 3");
        var numberSelector = new NumberSelector();

        var step3Future = startFuture.thenApplyAsync(numberSelector);

        var waitFuture = CompletableFuture.allOf(step1Future, write2Future, step3Future);

        var finalFuture = waitFuture.thenAcceptAsync(
                v -> log.info("Main: The CompletableFuture example has been completed"));

        finalFuture.join();
    }
}
