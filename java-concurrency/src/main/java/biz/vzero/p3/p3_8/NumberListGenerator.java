package biz.vzero.p3.p3_8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * @author duzeng
 * @className NumberListGenerator.java
 * @description TODO
 * @date 2024年08月10日 12:48
 */
public class NumberListGenerator implements Supplier<List<Long>> {

    private static final Logger log = LoggerFactory.getLogger(NumberListGenerator.class);

    private final int size;

    public NumberListGenerator(int size) {
        this.size = size;
    }

    @Override
    public List<Long> get() {
        var ret = new ArrayList<Long>();
        log.info("{} : NumberListGenerator : Start", Thread.currentThread().getName());

        for (int i = 0; i < size * 1000000; i++) {
            long number = Math.round(Math.random() * Long.MAX_VALUE);
            ret.add(number);
        }

        log.info("{} : NumberListGenerator : End", Thread.currentThread().getName());

        return ret;
    }
}
