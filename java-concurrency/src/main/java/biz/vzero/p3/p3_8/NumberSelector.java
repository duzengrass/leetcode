package biz.vzero.p3.p3_8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

/**
 * @author duzeng
 * @className NumberSelector.java
 * @description TODO
 * @date 2024年08月10日 12:52
 */
public class NumberSelector implements Function<List<Long>, Long> {

    private static final Logger log = LoggerFactory.getLogger(NumberSelector.class);

    @Override
    public Long apply(List<Long> longs) {
        log.info("{} : Step 3 : Start", Thread.currentThread().getName());
        long max = longs.stream().max(Long::compareTo).get();
        long min = longs.stream().min(Long::compareTo).get();
        long ret = (max + min) / 2;

        log.info("{} : Step 3 : Result - {}", Thread.currentThread().getName(), ret);
        return ret;
    }
}

