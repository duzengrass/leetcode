package biz.vzero.p4.p4_6;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.Date;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author duzeng
 * @className ScheduledExecutorServiceApp.java
 * @description TODO
 * @date 2024年08月10日 21:18
 */
public class ScheduledExecutorServiceApp {
    private static final Logger log = LoggerFactory.getLogger(ScheduledExecutorServiceApp.class);

    public static void main(String[] args) {
        ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
        //        executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        log.info("Main: Starting at: {}", new Date());

        for (var i = 0; i < 5; i++) {
            var task = new ExecutableTask("Task " + i);
            executor.schedule(task, i + 1, TimeUnit.SECONDS);
        }

        executor.shutdown();

        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            log.error("Main: The executor has finished", e);
        }

        log.info("Main: Ends at: {}", new Date());
    }
}
