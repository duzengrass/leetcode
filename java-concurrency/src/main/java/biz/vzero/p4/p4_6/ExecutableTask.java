package biz.vzero.p4.p4_6;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * @author duzeng
 * @className ExecutableTask.java
 * @description TODO
 * @date 2024年08月10日 21:15
 */
public class ExecutableTask implements Callable<String> {

    private static final Logger log = LoggerFactory.getLogger(ExecutableTask.class);

    private final String name;

    public ExecutableTask(String name) {
        this.name = name;
    }

    @Override
    public String call() throws Exception {
        log.info("{}: Starting at : {}", name, new Date());
        return "Hello, world";
    }

}
