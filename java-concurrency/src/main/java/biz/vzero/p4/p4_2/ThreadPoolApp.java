package biz.vzero.p4.p4_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author duzeng
 * @className ThreadpoolApp.java
 * @description TODO
 * @date 2024年08月10日 19:13
 */
public class ThreadPoolApp {

    private static final Logger log = LoggerFactory.getLogger(ThreadPoolApp.class);

    public static void main(String[] args) {
        var server = new Server();
        log.info("Main: Starting");
        for (int i = 0; i < 100; i++) {
            var task = new Task("Task " + i);
            server.executeTask(task);
        }
        log.info("Main: Shutting down the executor");
        server.endServer();

        log.info("Main: Sending another task");
        var task = new Task("Rejected task");
        server.executeTask(task);
        log.info("Main: End");
    }
}
