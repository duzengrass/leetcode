package biz.vzero.p4.p4_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @author duzeng
 * @className Task.java
 * @description TODO
 * @date 2024年08月10日 19:06
 */
public class Task implements Runnable{
    private static final Logger log = LoggerFactory.getLogger(Task.class);

    private final Date initDate;
    private final String name;

    public Task(String name) {
        this.initDate = new Date();
        this.name = name;
    }

    @Override
    public void run() {
        log.info("{}: Task {} : Created on: {}", Thread.currentThread().getName(), name, initDate);
        log.info("{}: Task {} : Started on: {}", Thread.currentThread().getName(), name, new Date());
        try {
            long duration = (long) (Math.random() * 10);
            log.info("{}: Task {} : Doing a task during {} seconds", Thread.currentThread().getName(), name, duration);
            Thread.sleep(duration * 1000);
        } catch (InterruptedException e) {
            log.error("{}: Task {} : Interrupted", Thread.currentThread().getName(), name);
            return;
        }
        log.info("{}: Task {} : Finished on: {}", Thread.currentThread().getName(), name, new Date());
    }
}
