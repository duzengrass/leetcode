package biz.vzero.p4.p4_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.RejectedExecutionHandler;

/**
 * @author duzeng
 * @className RejectedTaskController.java
 * @description TODO
 * @date 2024年08月10日 19:07
 */
public class RejectedTaskController implements RejectedExecutionHandler {
    private static final Logger log = LoggerFactory.getLogger(RejectedTaskController.class);

    @Override
    public void rejectedExecution(Runnable r, java.util.concurrent.ThreadPoolExecutor executor) {
        log.error("RejectedTaskController: The task {} has been rejected", r.toString());
        log.info("RejectedTaskController: {} ", executor.toString());
        log.info("RejectedTaskController: Terminating: {}", executor.isTerminating());
        log.info("RejectedTaskController: Terminated: {}", executor.isTerminated());
    }
}
