package biz.vzero.p4.p4_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author duzeng
 * @className Server.java
 * @description TODO
 * @date 2024年08月10日 19:10
 */
public class Server {
    private static final Logger log = LoggerFactory.getLogger(Server.class);
    private final ThreadPoolExecutor executor;

    public Server() {
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        var controller = new RejectedTaskController();
        executor.setRejectedExecutionHandler(controller);
    }

    public void executeTask(Task task) {
        log.info("Server: A new task has arrived");
        executor.execute(task);
        log.info("Server: Pool size: {}", executor.getPoolSize());
        log.info("Server: Active count: {}", executor.getActiveCount());
        log.info("Server: Task count: {}", executor.getTaskCount());
        log.info("Server: Completed task count: {}", executor.getCompletedTaskCount());
    }


    public void endServer() {
        executor.shutdown();
    }

}
