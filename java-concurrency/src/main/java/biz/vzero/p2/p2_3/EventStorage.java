package biz.vzero.p2.p2_3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author duzeng
 * @className EventStorage.java
 * @description TODO
 * @date 2024年08月01日 18:40
 */
public class EventStorage {
    private static final Logger log = LoggerFactory.getLogger(EventStorage.class);

    private int maxSize;
    private Queue<Date> storage;

    public EventStorage() {
        maxSize = 10;
        storage = new LinkedList<>();
    }

    public synchronized void set() {
        while (storage.size() == maxSize) {
            try {
                wait();
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }
        storage.offer(new Date());
        log.info("Set: {}", storage.size());
        notify();
    }

    public synchronized void get() {
        while (storage.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }

        log.info("Get: {}: {}", storage.size(), storage.poll().toString());
        notify();
    }
}
