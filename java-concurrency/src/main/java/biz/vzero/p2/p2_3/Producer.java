package biz.vzero.p2.p2_3;

/**
 * @author duzeng
 * @className Producer.java
 * @description TODO
 * @date 2024年08月01日 18:45
 */
public class Producer implements Runnable{
    private EventStorage storage;
    public Producer(EventStorage storage) {
        this.storage = storage;
    }
    @Override
    public void run() {
       for (var i = 0; i < 100; i++) {
           storage.set();
       }
    }
}
