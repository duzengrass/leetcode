package biz.vzero.p2.p2_3;

/**
 * @author duzeng
 * @className Consumer.java
 * @description TODO
 * @date 2024年08月01日 18:45
 */
public class Consumer implements Runnable {
    private EventStorage storage;

    public Consumer(EventStorage storage) {
        this.storage = storage;
    }

    @Override
    public void run() {
        for (var i = 0; i < 100; i++) {
            storage.get();
        }
    }
}
