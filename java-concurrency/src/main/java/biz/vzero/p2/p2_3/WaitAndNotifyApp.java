package biz.vzero.p2.p2_3;

/**
 * @author duzeng
 * @className WaitAndNotifyApp.java
 * @description TODO
 * @date 2024年08月01日 18:46
 */
public class WaitAndNotifyApp {
    public static void main(String[] args) {
        var storage = new EventStorage();
        var producer = new Producer(storage);
        var consumer = new Consumer(storage);
        var thread1 = new Thread(producer);
        var thread2 = new Thread(consumer);
        thread1.start();
        thread2.start();
    }
}
