package biz.vzero.p2.p2_5;

/**
 * @author duzeng
 * @className ReadWriteLockApp.java
 * @description TODO
 * @date 2024年08月03日 11:03
 */
public class ReadWriteLockApp {
    public static void main(String[] args) {
        var pricesInfo = new PricesInfo();
        var readers = new Reader[5];
        var threadsReader = new Thread[5];

        for (var i = 0; i < 5; i++) {
            readers[i] = new Reader(pricesInfo);
            threadsReader[i] = new Thread(readers[i]);
        }

        var writer = new Writer(pricesInfo);
        var threadWriter = new Thread(writer);

        for (var i = 0; i < 5; i++) {
            threadsReader[i].start();
        }
        threadWriter.start();
    }
}
