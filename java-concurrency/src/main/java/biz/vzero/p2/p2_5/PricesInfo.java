package biz.vzero.p2.p2_5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author duzeng
 * @className PricesInfo.java
 * @description TODO
 * @date 2024年08月03日 10:56
 */
public class PricesInfo {
    private static final Logger log = LoggerFactory.getLogger(PricesInfo.class);
    private double price1;
    private double price2;
    private ReadWriteLock lock;

    public PricesInfo() {
        price1 = 1.0;
        price2 = 2.0;
        lock = new ReentrantReadWriteLock();
    }

    public double getPrice1() {
        lock.readLock().lock();
        var value = price1;
        lock.readLock().unlock();
        return value;
    }

    public double getPrice2() {
        lock.readLock().lock();
        var value = price2;
        lock.readLock().unlock();
        return value;

    }

    public void setPrices(double price1, double price2) {
        lock.writeLock().lock();
        log.info("{}: PricesInfo: Write Lock acquired", Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        }
        this.price1 = price1;
        this.price2 = price2;
        log.info("{}: PricesInfo: Write Lock released", Thread.currentThread().getName());
        lock.writeLock().unlock();
    }
}
