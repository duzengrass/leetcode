package biz.vzero.p2.p2_5;

/**
 * @author duzeng
 * @className Writer.java
 * @description TODO
 * @date 2024年08月03日 11:02
 */
public class Writer implements Runnable {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Writer.class);
    private final PricesInfo pricesInfo;

    public Writer(PricesInfo pricesInfo) {
        this.pricesInfo = pricesInfo;
    }

    @Override
    public void run() {
        for (var i = 0; i < 3; i++) {
            log.info("Writer: Attempt to modify the prices");
            pricesInfo.setPrices(Math.random() * 10, Math.random() * 8);
            log.info("Writer: Prices have been modified");
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }
    }
}
