package biz.vzero.p2.p2_5;

/**
 * @author duzeng
 * @className Reader.java
 * @description TODO
 * @date 2024年08月03日 11:00
 */
public class Reader implements Runnable {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Reader.class);
    private final PricesInfo pricesInfo;

    public Reader(PricesInfo pricesInfo) {
        this.pricesInfo = pricesInfo;
    }

    @Override
    public void run() {
        for (var i = 0; i < 20; i++) {
            log.info("{}: Price 1: {}", Thread.currentThread().getName(), pricesInfo.getPrice1());
            log.info("{}: Price 2: {}", Thread.currentThread().getName(), pricesInfo.getPrice2());
        }
    }
}
