package biz.vzero.p2.p2_4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author duzeng
 * @className LockApp.java
 * @description TODO
 * @date 2024年08月03日 10:01
 */
public class LockApp {
    private static final Logger log = LoggerFactory.getLogger(LockApp.class);

    public static void main(String[] args) {
        log.info("Running example with fair-mode = false");
        testPrintQueue(false);
        log.info("Running example with fair-mode = true");
        testPrintQueue(true);
    }

    private static void testPrintQueue(boolean fairMode) {
        var printQueue = new PrintQueue(fairMode);
        var threads = new Thread[10];
        for (var i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new Job(printQueue), "Thread " + i);
        }
        for (var i = 0; i < threads.length; i++) {
            threads[i].start();
        }
        for (var i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                log.error("Interrupted", e);
            }
        }
    }
}
