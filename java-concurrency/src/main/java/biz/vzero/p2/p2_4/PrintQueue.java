package biz.vzero.p2.p2_4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author duzeng
 * @className PrintQueue.java
 * @description TODO
 * @date 2024年08月03日 09:55
 */
public class PrintQueue {
    private static final Logger log = LoggerFactory.getLogger(PrintQueue.class);

    private Lock queueLock;

    public PrintQueue(boolean fairMode) {
        queueLock = new ReentrantLock(fairMode);
    }

    public void printJob(Object document) {
        queueLock.lock();
        try {
            var duration = (long) (Math.random() * 10000);
            log.info("{}: PrintQueue: Printing a Job during {} seconds", Thread.currentThread().getName(),
                    duration / 1000);
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        } finally {
            queueLock.unlock();
        }
    }
}
