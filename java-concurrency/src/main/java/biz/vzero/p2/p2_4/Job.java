package biz.vzero.p2.p2_4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author duzeng
 * @className Job.java
 * @description TODO
 * @date 2024年08月03日 09:57
 */
public class Job implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(Job.class);
    private PrintQueue printQueue;

    public Job(PrintQueue printQueue) {
        this.printQueue = printQueue;
    }

    @Override
    public void run() {
        log.info("{}: Going to print a document", Thread.currentThread().getName());
        printQueue.printJob(new Object());
        log.info("{}: The document has been printed", Thread.currentThread().getName());
    }
}
