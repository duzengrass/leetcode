package biz.vzero.p2.p2_6;

/**
 * @author duzeng
 * @className FileMock.java
 * @description TODO
 * @date 2024年08月03日 17:34
 */
public class FileMock {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(FileMock.class);
    private final String[] content;
    private int index;

    public FileMock(int size, int length) {
        content = new String[size];
        for (var i = 0; i < size; i++) {
            var buffer = new StringBuilder(length);
            for (var j = 0; j < length; j++) {
                var randomCharacter = (int) (Math.random() * 255);
                buffer.append((char) randomCharacter);
            }
            content[i] = buffer.toString();
        }
        index = 0;
    }

    public boolean hasMoreLines() {
        return index < content.length;
    }

    public String getLine() {
        if (this.hasMoreLines()) {
            log.info("Mock: {}", (content.length - index));
            return content[index++];
        }
        return null;
    }
}
