package biz.vzero.p2.p2_6;

/**
 * @author duzeng
 * @className LockAndConditionApp.java
 * @description TODO
 * @date 2024年08月03日 17:45
 */
public class LockAndConditionApp {
    public static void main(String[] args) {

        var buffer = new Buffer(20);
        var producer = new Producer(new FileMock(100, 10), buffer);
        var thread1 = new Thread(producer, "Producer");

        var consumerThreads = new Thread[3];
        for (var i = 0; i < 3; i++) {
            var consumer = new Consumer(buffer);
            consumerThreads[i] = new Thread(consumer, "Consumer " + i);
        }

        thread1.start();

        for (int i = 0; i < 3; i++) {
            consumerThreads[i].start();
        }
    }
}
