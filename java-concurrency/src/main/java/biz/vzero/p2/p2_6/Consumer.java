package biz.vzero.p2.p2_6;

import java.util.Random;

/**
 * @author duzeng
 * @className Consumer.java
 * @description TODO
 * @date 2024年08月03日 17:43
 */
public class Consumer implements Runnable {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Consumer.class);
    private final Buffer buffer;

    public Consumer(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while (buffer.hasPendingLines()) {
            var line = buffer.get();
            processLine(line);
        }
    }

    private void processLine(String line) {
        try {
            Random random = new Random();
            Thread.sleep(random.nextInt(100));
            log.info("{}: Processing line: {}", Thread.currentThread().getName(), line);
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        }
    }
}
