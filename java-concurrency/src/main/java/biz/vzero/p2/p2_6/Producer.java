package biz.vzero.p2.p2_6;

/**
 * @author duzeng
 * @className Producer.java
 * @description TODO
 * @date 2024年08月03日 17:42
 */
public class Producer implements Runnable{

    private final FileMock mock;
    private final Buffer buffer;

    public Producer(FileMock mock, Buffer buffer) {
        this.mock = mock;
        this.buffer = buffer;
    }

    @Override
    public void run() {
        buffer.setPendingLines(true);
        while (mock.hasMoreLines()) {
            var line = mock.getLine();
            buffer.insert(line);
        }
        buffer.setPendingLines(false);
    }
}
