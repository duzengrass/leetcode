package biz.vzero.heap;

import biz.vzero.heap.HeapSort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className HeapSortTest.java
 * @description TODO
 * @date 2023年10月19日 11:17
 */
class HeapSortTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{3, 8, 12, 1, 2}, new int[]{1, 2, 3, 8, 12}),
                Arguments.of(new int[]{1, 2, 2, 2, 3, 3, 3, 3, 5, 4, 9, 1, 2, 1, 3, 2}, new int[]{1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 5, 9})
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void sort(int[] nums, int[] expected) {
        var obj=new HeapSort(nums);
        obj.sort();
        assertArrayEquals(expected,nums);
    }

    @Test
    void other() {
        assertEquals(1, 3 / 2);
        assertEquals(2, 5 / 2);
        assertEquals(3, 7 / 2);
        assertEquals(4, 9 / 2);
        assertEquals(5, 11 / 2);
        assertEquals(2, 7 / 3);

        assertEquals(1, 5 / 2 - 1);
    }
}