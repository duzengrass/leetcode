package biz.vzero.heap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className KthLargestTest.java
 * @description TODO
 * @date 2023年10月18日 18:42
 */
class KthLargestTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{3, 2, 1, 5, 6, 4}, 2, 5),
                Arguments.of(new int[]{3, 2, 3, 1, 2, 4, 5, 5, 6}, 4, 4),
                Arguments.of(new int[]{1}, 1, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void findKthLargest(int[] nums, int k, int expected) {
        var obj = new KthLargest();
        var actual = obj.findKthLargest(nums, k);
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("provide")
    void findKthLargestUseQuickSort(int[] nums, int k, int expected) {
        var obj = new KthLargest();
        var actual = obj.findKthLargestUseQuickSort(nums, k);
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("provide")
    void findKthLargestUseHeapSort(int[] nums, int k, int expected) {
        var obj = new KthLargest();
        var actual = obj.findKthLargestUseHeapSort(nums, k);
        assertEquals(expected, actual);
    }
}