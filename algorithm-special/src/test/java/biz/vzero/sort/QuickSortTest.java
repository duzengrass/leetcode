package biz.vzero.sort;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className QuickSortTest.java
 * @description TODO
 * @date 2023年10月18日 19:24
 */
class QuickSortTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{3, 8, 12, 1, 2}, new int[]{1, 2, 3, 8, 12}),
                Arguments.of(new int[]{1, 2, 2, 2, 3, 3, 3, 3, 5, 4, 9, 1, 2, 1, 3, 2}, new int[]{1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 5, 9})
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void sort(int[] nums, int[] expected) {
        var obj = new QuickSort();
        obj.sort(nums);
        assertArrayEquals(expected, nums);
    }
}