package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className BinarySearchTest.java
 * @description TODO
 * @date 2023年10月10日 20:52
 */
class BinarySearchTest {

    private static Stream<Arguments> provide() {
        return Stream.of(Arguments.of(new int[] { -1, 0, 3, 5, 9, 12 }, 9, 4),
                Arguments.of(new int[] { -1, 0, 3, 5, 9, 12 }, 2, -1));
    }

    @ParameterizedTest
    @MethodSource("provide")
    void search(int[] nums, int target, int expected) {
        var index = new BinarySearch().search(nums, target);
        assertEquals(expected, index);
    }

    @Test
    void whatever() {
        sync();
        assertEquals(1, 1);
    }

    public void sync() {
        synchronized (this) {
            System.out.println("sync");
        }
    }

    public synchronized void sync2() {
        System.out.println("sync2");
    }
}