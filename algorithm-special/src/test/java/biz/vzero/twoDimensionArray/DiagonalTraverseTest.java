package biz.vzero.twoDimensionArray;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className DiagonalTraverseTest.java
 * @description TODO
 * @date 2023年10月22日 18:17
 */
class DiagonalTraverseTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[][]{new int[]{1, 2, 3}, new int[]{4, 5, 6}, new int[]{7, 8, 9}}, new int[]{1, 2, 4, 7, 5, 3, 6, 8, 9}),
                Arguments.of(new int[][]{new int[]{1, 2}, new int[]{3, 4}}, new int[]{1, 2, 3, 4})
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void findDiagonalOrder(int[][]mat, int[] expected) {
        var obj=new DiagonalTraverse();
        var actual=obj.findDiagonalOrder(mat);
        assertArrayEquals(expected,actual);
    }
}