package biz.vzero;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className MedianTwoSortedArrayTest.java
 * @description TODO
 * @date 2023年10月23日 00:20
 */
class MedianTwoSortedArrayTest {
    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[] {1,3},new int[]{2},2.0),
                Arguments.of(new int[]{1, 2}, new int[]{3, 4}, 2.5)
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void findMedianSortedArrays(int[] nums1, int[] nums2, double expected) {
        var obj = new MedianTwoSortedArray();
        var actual = obj.findMedianSortedArrays(nums1, nums2);
        assertEquals(expected, actual);
    }
}