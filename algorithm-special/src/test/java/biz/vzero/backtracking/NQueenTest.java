package biz.vzero.backtracking;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className NQueenTest.java
 * @description TODO
 * @date 2023年10月17日 15:36
 */
class NQueenTest {
    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(4, List.of(List.of(".Q..","...Q","Q...","..Q."), List.of("..Q.", "Q...", "...Q", ".Q.."))),
                Arguments.of(1, List.of(List.of("Q")))
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void solveNQueens(int n, List<List<String>> expected) {
        var nq = new NQueen();
        var actual = nq.solveNQueens(n);
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {
            assertArrayEquals(expected.get(i).toArray(new String[0]), actual.get(i).toArray(new String[0]));
        }
    }
}