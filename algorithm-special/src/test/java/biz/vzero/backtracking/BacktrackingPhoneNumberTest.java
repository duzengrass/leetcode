package biz.vzero.backtracking;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className BacktrackingPhoneNumberTest.java
 * @description TODO
 * @date 2023年10月16日 18:19
 */
class BacktrackingPhoneNumberTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of("23", new String[]{"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}),
                Arguments.of("", new String[]{}),
                Arguments.of("2", new String[]{"a", "b", "c"})
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void letterCombinations(String digits, String[] expected) {
        var solution = new BacktrackingPhoneNumber();
        var actual = solution.letterCombinations(digits);
        assertArrayEquals(expected, actual.toArray(new String[0]));
    }
    @ParameterizedTest
    @MethodSource("provide")
    void letterCombinations2(String digits, String[] expected) {
        var solution = new BacktrackingPhoneNumber();
        var actual = solution.letterCombinations2(digits);
        assertArrayEquals(expected, actual.toArray(new String[0]));
    }
}