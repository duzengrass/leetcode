package biz.vzero.string;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className LongestSubStrTest.java
 * @description TODO
 * @date 2023年10月21日 09:02
 */
class LongestSubStrTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of("abcde", "ace", 3),
                Arguments.of("abc", "abc", 3),
                Arguments.of("abc", "def", 0),
                Arguments.of("psnw","vozsh",1),
                Arguments.of("oxcpqrsvwf","shmtulqrypy",2),
                Arguments.of("ezupkr","ubmrapg",2),
                Arguments.of("pcbmdupybalwpkbidypqbwhefijytypwdwbsharqdurkrslqlqla","jodcpirubsryvudgpwncrmtypatunqpkhehuhkdmbctyxghsfktaz",15)
        );

    }

    @ParameterizedTest
    @MethodSource("provide")
    void longestCommonSubsequence(String text1, String text2, int expected) {
        var obj = new LongestSubStr();
        var actual = obj.longestCommonSubsequence(text1, text2);
        assertEquals(expected, actual);
    }
}