package biz.vzero.string;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className MaxSubarrayTest.java
 * @description TODO
 * @date 2023年10月22日 11:09
 */
class MaxSubarrayTest {
    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}, 6),
                Arguments.of(new int[]{1}, 1),
                Arguments.of(new int[]{5, 4, -1, 7, 8}, 23)
        );

    }

    @ParameterizedTest
    @MethodSource("provide")
    void maxSubArray(int[] nums, int expected) {
        var obj = new MaxSubarray();
        var actual = obj.maxSubArrayUseDivideConquer(nums);
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("provide")
    void maxSubArrayUseDynamicProgramming(int[] nums, int expected) {
        var obj = new MaxSubarray();
        var actual = obj.maxSubArrayUseDynamicProgramming(nums);
        assertEquals(expected, actual);
    }
}