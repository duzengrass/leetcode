package biz.vzero.greedy;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className DistributeCandyTest.java
 * @description TODO
 * @date 2023年10月17日 15:55
 */
class DistributeCandyTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{1, 0, 2}, 5),
                Arguments.of(new int[]{1, 2, 2}, 4),
                Arguments.of(new int[]{1, 3, 2, 2, 1}, 7),
                Arguments.of(new int[]{1, 2, 87, 87, 87, 2, 1}, 13),
                Arguments.of(new int[]{1,3,4,5,2},11)
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void candy(int[] ratings, int expected) {
        var d = new DistributeCandy();
        var actual = d.candy(ratings);
        assertEquals(expected, actual);
    }
}