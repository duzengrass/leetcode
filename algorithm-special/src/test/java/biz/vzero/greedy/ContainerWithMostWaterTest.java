package biz.vzero.greedy;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className ContainerWithMostWaterTest.java
 * @description TODO
 * @date 2023年10月18日 16:48
 */
class ContainerWithMostWaterTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}, 49)
                , Arguments.of(new int[]{1, 1}, 1),
                Arguments.of(new int[]{2, 3, 4, 5, 18, 17, 6}, 17)
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void maxArea(int[] height, int expected) {
        var a = new ContainerWithMostWater();
        var actual = a.maxArea(height);
        assertEquals(expected, actual);
    }
}