package biz.vzero.slide;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className LongestSubarrLessThanLimitTest.java
 * @description TODO
 * @date 2023年10月23日 10:47
 */
class LongestSubarrLessThanLimitTest {

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{4, 2, 2, 2, 4, 4, 2, 2}, 0, 3)
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void longestSubarray(int[] nums, int limit, int expected) {
        var obj = new LongestSubarrLessThanLimit();
        var actual = obj.longestSubarray(nums, limit);
        assertEquals(expected, actual);
    }
}