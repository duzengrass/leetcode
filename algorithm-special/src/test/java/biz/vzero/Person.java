package biz.vzero;

/**
 * @className Person.java
 * @author duzeng
 * @description TODO
 * @date 2024年01月03日 22:13
 */
public class Person implements  Cloneable{
    @Override
    public Person clone() {
        try {
            return (Person) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
