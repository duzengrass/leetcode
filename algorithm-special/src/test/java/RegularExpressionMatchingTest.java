import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.ArgumentUtils;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className RegularExpressionMatchingTest.java
 * @description TODO
 * @date 2023年10月22日 19:31
 */
class RegularExpressionMatchingTest {
    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of("aa", "a", false),
                Arguments.of("aa", "a*", true),
                Arguments.of("ab", ".*", true),
                Arguments.of("aab", "c*a*b", true),
                Arguments.of("mississippi", "mis*is*p*.", false),
                Arguments.of("ab", ".*c", false),
                Arguments.of("aaa", "aaaa", false),
                Arguments.of("aaa", "a*a", true)
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void isMatch(String s, String p, boolean expected) {
        var obj = new RegularExpressionMatching();
        var actual = obj.isMatch(s, p);
        assertEquals(expected, actual);
        ClassLoader.getSystemClassLoader();
    }
}