/**
 * @author duzeng
 * @className RegularExpressionMatching.java
 * @description TODO
 * @date 2023年10月22日 19:14
 */
public class RegularExpressionMatching {
    public boolean isMatch(String s, String p) {
        var m = s.length();
        var n = p.length();
        var dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int i = 0; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (p.charAt(j - 1) == '*') {
                    dp[i][j] = dp[i][j - 2];
                    if (matches(s, p, i, j - 1)) {
                        dp[i][j] = dp[i][j] || dp[i - 1][j];
                    }
                } else {
                    if (matches(s, p, i, j)) {
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                }
            }
        }
        return dp[m][n];
    }

    private boolean matches(String s, String p, int i, int j) {
        if (i == 0) return false;
        if (p.charAt(j - 1) == '.') return true;
        return s.charAt(i - 1) == p.charAt(j - 1);
    }

//    private boolean match(String s, String p, int sIndex, int pIndex) {
//        if (sIndex >= s.length()) {
//            if (pIndex == p.length() - 2 && p.charAt(pIndex + 1) == s.charAt(sIndex - 1)) {
//                return true;
//            }
//            if (pIndex < p.length() - 1) return false;
//            if (pIndex >= p.length()) return true;
//            if (p.charAt(pIndex) != '*') return false;
//            return true;
//        }
//        if (pIndex >= p.length()) return false;
//        if (s.charAt(sIndex) == p.charAt(pIndex) || p.charAt(pIndex) == '.') {
//            return match(s, p, sIndex + 1, pIndex + 1);
//        }
//
//        if (p.charAt(pIndex) == '*') {
//            if (p.charAt(pIndex - 1) == s.charAt(sIndex) || p.charAt(pIndex - 1) == '.') {
//                return match(s, p, sIndex + 1, pIndex);
//            }
//        }
//
//        return match(s, p, sIndex, pIndex + 1);
//
//    }
}
