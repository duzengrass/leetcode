package biz.vzero;

import javax.swing.tree.TreeNode;

/**
 * @author duzeng
 * @className BinanryTreeMaxPath.java
 * @description TODO
 * @date 2023年10月23日 07:28
 */
public class BinanryTreeMaxPath {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    private int sum = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
         max(root);
         return sum;
    }

    private int max(TreeNode node) {
        if (node == null) return 0;

        var left = Math.max(max(node.left), 0);
        var right = Math.max(max(node.right), 0);

        var tempsum = node.val + left + right;
        sum = Math.max(sum, tempsum);
        return node.val + Math.max(left, right);
    }
}
