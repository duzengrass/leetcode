package biz.vzero.greedy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author duzeng
 * @className ContainerWithMostWater.java
 * @description leetcode-0011
 * @date 2023年10月18日 16:47
 */
public class ContainerWithMostWater {
    public int maxArea(int[] height) {
        int result = 0;
        int left = 0, right = height.length - 1;
        while (left < right) {
            result = Math.max(result, area(left, height[left], right, height[right]));
            if (height[left] <= height[right]) {
                left++;
            } else {
                right--;
            }
        }

        return result;
    }

    private int area(int x1, int y1, int x2, int y2) {
        return Math.abs(x1 - x2) * Math.min(y1, y2);
    }

}
