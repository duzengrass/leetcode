package biz.vzero.heap;

/**
 * @author duzeng
 * @className HeapSort.java
 * @description TODO
 * @date 2023年10月19日 11:15
 */
public class HeapSort {
    private MyHeap heap;

    public HeapSort(int[] nums) {
        heap = new MyHeap(nums);
    }

    public void sort() {
        heap.buildMaxHeap();
        for (int i = heap.arraySize(); i >= 2; i--) {
            heap.swap(1, i);
            heap.setHeapSize(heap.getHeapSize() - 1);
            heap.maxHeapIfy(1);
        }
    }


}
