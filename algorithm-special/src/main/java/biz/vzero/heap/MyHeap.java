package biz.vzero.heap;

/**
 * @author duzeng
 * @className MyHeap.java
 * @description TODO
 * @date 2023年10月19日 11:21
 */
public class MyHeap {
    public MyHeap(int[] nums) {
        this.nums = nums;
    }

    private int[] nums;

    public int arraySize() {
        return nums.length;
    }

    public int getHeapSize() {
        return heapSize;
    }

    public MyHeap setHeapSize(int heapSize) {
        this.heapSize = heapSize;
        return this;
    }

    private int heapSize;

    private int parent(int i) {
        return i / 2;
    }

    private int left(int i) {
        return i * 2;
    }

    private int right(int i) {
        return i * 2 + 1;
    }

    public void maxHeapIfy(int i) {
        int left = left(i);
        int right = right(i);
        int largest;
        if (left <= heapSize && nums[left - 1] > nums[i - 1]) {
            largest = left;
        } else {
            largest = i;
        }
        if (right <= heapSize && nums[right - 1] > nums[largest - 1]) {
            largest = right;
        }
        if (largest != i) {
            swap(i, largest);
            maxHeapIfy(largest);
        }

    }

    public void swap(int x, int y) {
        var temp = nums[x - 1];
        nums[x - 1] = nums[y - 1];
        nums[y - 1] = temp;
    }

    public void buildMaxHeap() {
        this.heapSize = this.nums.length;
        for (int i = heapSize / 2; i >= 1; i--) {
            maxHeapIfy(i);
        }
    }

    public int heapMax() {
        return nums[0];
    }

    public int heapExtractMax() throws Exception {
        if (heapSize < 1) {
            throw new Exception("underflow");
        }
        var max = heapMax();
        nums[0] = nums[heapSize - 1];
        heapSize = heapSize - 1;
        maxHeapIfy(1);
        return max;
    }
    private void heapIncreaseKey(int i,int value) throws Exception {
        if (value<nums[i-1]) throw new Exception("new value is smaller than current.");
        nums[i-1]=value;
        while (i>1 && nums[parent(i)-1]<nums[i-1]) {
            swap(i,parent(i));
            i=parent(i);
        }
    }

    public void insert(int value) throws Exception {
        heapSize=heapSize+1;
        nums[heapSize-1]=Integer.MIN_VALUE;
        heapIncreaseKey(heapSize,value);
    }
}
