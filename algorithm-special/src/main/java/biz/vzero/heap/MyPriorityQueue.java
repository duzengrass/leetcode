package biz.vzero.heap;

/**
 * @author duzeng
 * @className MyPriorityQueue.java
 * @description TODO
 * @date 2023年10月19日 17:02
 */
public class MyPriorityQueue {
    private MyHeap heap;


    public MyPriorityQueue(int[] nums) {
        this.heap=new MyHeap(nums);
    }

    public int poll() throws Exception {
        return this.heap.heapExtractMax();
    }
    public void offer(int value) throws Exception {
        this.heap.insert(value);
    }
}
