package biz.vzero.heap;

import java.util.Collections;
import java.util.PriorityQueue;


/**
 * @author duzeng
 * @className KthLargest.java
 * @description leetcode-0215
 * @date 2023年10月18日 18:41
 */
public class KthLargest {
    public int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> heap = new PriorityQueue<>(Collections.reverseOrder());
        for (var item : nums) {
            heap.add(item);
        }
        var index = 1;
        while (index < k) {
            heap.poll();
            index++;
        }

        return heap.poll();
    }

    public int findKthLargestUseQuickSort(int[] nums, int k) {
        return find(nums, 0, nums.length - 1, k);
    }

    private int find(int[] nums, int left, int right, int k) {
        if (left == right) return nums[left];

        if (left < right) {
            var q = partition(nums, left, right);
            if (q < nums.length - k) {
                return find(nums, q + 1, right, k);
            } else if (q > nums.length - k) {
                return find(nums, left, q - 1, k);
            }
            return nums[q];
        }
        return -1;
    }

    private int partition(int[] nums, int left, int right) {
        var pivot = nums[right];
        var sIndex = left - 1;
        for (int i = left; i <= right - 1; i++) {
            if (nums[i] < pivot) {
                sIndex++;
                swap(nums, i, sIndex);
            }
        }
        swap(nums, sIndex + 1, right);
        return sIndex + 1;
    }

    private void swap(int[] nums, int x, int y) {
        var temp = nums[x];
        nums[x] = nums[y];
        nums[y] = temp;
    }

    public int findKthLargestUseHeapSort(int[] nums, int k) {
        return find2(nums, k);
    }

    private int heapSize;

    private int find2(int[] nums, int k) {
        buildMaxHeap(nums);
        for (int i = nums.length; i > nums.length - k + 1; i--) {
            swap(nums, 1 - 1, i - 1);
            heapSize = heapSize - 1;
            maxHeapIfy(nums, 1);
        }
        return nums[0];
    }

    private void buildMaxHeap(int[] nums) {
        this.heapSize = nums.length;
        for (int i = nums.length / 2; i >= 1; i--) {
            maxHeapIfy(nums, i);
        }
    }

    private int left(int i) {
        return 2 * i;
    }

    private int right(int i) {
        return 2 * i + 1;
    }

    private void maxHeapIfy(int[] nums, int i) {

        var left = left(i);
        var right = right(i);
        var largest = i;
        if (left <= heapSize && nums[left - 1] > nums[i - 1]) {
            largest = left;
        }
        if (right <= heapSize && nums[right - 1] > nums[largest - 1]) {
            largest = right;
        }

        if (largest != i) {
            swap(nums, i - 1, largest - 1);
            maxHeapIfy(nums, largest);
        }
    }
}
