package biz.vzero.string;

/**
 * @author duzeng
 * @className MaxSubarray.java
 * @description TODO
 * @date 2023年10月22日 11:08
 */
public class MaxSubarray {
    public int maxSubArrayUseDynamicProgramming(int[] nums) {
        var dp = new int[nums.length];
        dp[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (dp[i - 1] > 0) {
                dp[i] = dp[i - 1] + nums[i];
            } else {
                dp[i] = nums[i];
            }
        }

        var result = Integer.MIN_VALUE;
        for (var item : dp) {
            result = Math.max(item, result);
        }

        return result;
    }

    public int maxSubArrayUseDivideConquer(int[] nums) {
        return findMaxSubarray(nums, 0, nums.length - 1);
    }

    private int findMaxSubarray(int[] nums, int low, int high) {
        if (low == high) return nums[low];

        var mid = (low + high) / 2;
        var leftSum = findMaxSubarray(nums, low, mid);
        var rightSum = findMaxSubarray(nums, mid + 1, high);
        var crossSum = findMaxCrossSubarray(nums, low, mid, high);

        return Math.max(Math.max(leftSum, rightSum), crossSum);
    }

    private int findMaxCrossSubarray(int[] nums, int low, int mid, int high) {
        var leftSum = Integer.MIN_VALUE;
        var sum = 0;
        for (int i = mid; i >= low; i--) {
            sum = sum + nums[i];
            if (sum > leftSum) {
                leftSum = sum;
            }
        }
        sum = 0;
        var rightSUm = Integer.MIN_VALUE;
        for (int i = mid + 1; i <= high; i++) {
            sum = sum + nums[i];
            if (sum > rightSUm) {
                rightSUm = sum;
            }
        }

        return leftSum + rightSUm;
    }
}
