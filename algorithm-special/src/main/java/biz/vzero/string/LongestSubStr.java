package biz.vzero.string;

/**
 * @author duzeng
 * @className LongestSubStr.java
 * @description leetcode-1143
 * @date 2023年10月20日 09:53
 */
public class LongestSubStr {
    public int longestCommonSubsequence(String text1, String text2) {
        var m = text1.length();
        var n = text2.length();
        var c = new int[m + 1][n + 1];
        for (int i = 0; i < m + 1; i++) {
            c[i][0] = 0;
        }
        for (int i = 0; i < n + 1; i++) {
            c[0][i] = 0;
        }

        for (int i = 1; i < m + 1; i++) {
            for (int j = 1; j < n + 1; j++) {
                if (text1.charAt(i - 1) == text2.charAt(j - 1)) {
                    c[i][j] = c[i - 1][j - 1] + 1;
                } else {
                    c[i][j] = Math.max(c[i - 1][j], c[i][j - 1]);
                }
            }
        }

        return c[m][n];
    }

    public int longestCommonSubsequence2(String text1, String text2) {
        if (text1.equals(text2)) {
            return text1.length();
        }
        if (text1.contains(text2) || text2.contains(text1)) {
            return Math.min(text1.length(), text2.length());
        }
        if (text1.length() > text2.length()) {
            return judge(text1, text2);
        } else if (text1.length() < text2.length()) {
            return judge(text2, text1);
        } else {
            return Math.max(judge(text1, text2), judge(text2, text1));
        }

    }

    private int judge(String parent, String child) {
        return judge(parent, child, 0, 0);
    }

    private int judge(String parent, String child, int sIndex, int pIndex) {
        if (sIndex >= child.length() || pIndex >= parent.length()) return 0;

        var longest = 0;
        for (int i = sIndex; i < child.length(); i++) {
            var temp = child.charAt(i);
            var j = pIndex;
            while (j < parent.length()) {
                if (temp == parent.charAt(j)) {
                    longest = Math.max(longest, 1 + judge(parent, child, i + 1, j + 1));
                }
                j++;
            }
        }
        return longest;
    }
}
