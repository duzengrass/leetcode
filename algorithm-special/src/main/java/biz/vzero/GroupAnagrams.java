package biz.vzero;

import java.util.*;

/**
 * @author duzeng
 * @className GroupAnagrams.java
 * @description TODO
 * @date 2023年10月23日 07:56
 */
public class GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> existed = new HashMap<>();
        for (var item : strs
        ) {
            var charArray = item.toCharArray();
            Arrays.sort(charArray);
            var key = new String(charArray);
            var list = existed.getOrDefault(key, new ArrayList<>());
            list.add(item);
            existed.put(key, list);
        }
        return new ArrayList<List<String>>(existed.values());
    }
}
