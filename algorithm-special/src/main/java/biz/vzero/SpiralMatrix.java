package biz.vzero;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author duzeng
 * @className SpiralMatrix.java
 * @description TODO
 * @date 2023年10月23日 11:48
 */
public class SpiralMatrix {
    public List<Integer> spiralOrder(int[][] matrix) {
        var list = new ArrayList<Integer>();
        var rows = matrix.length;
        var cols = matrix[0].length;
        var tag = new boolean[matrix.length][];
        var directions = new int[][]{new int[]{0, 1}, new int[]{1, 0}, new int[]{0, -1}, new int[]{-1, 0}};
        for (int i = 0; i < matrix.length; i++) {
            var a = new boolean[matrix[i].length];
            Arrays.fill(a, false);
            tag[i] = a;
        }

        var dIndex = 0;
        var direction = directions[dIndex];
        var row = 0;
        var col = 0;
        while (list.size() < cols * rows) {
            list.add(matrix[row][col]);
            tag[row][col] = true;
            var newRow = row + direction[0];
            var newCol = col + direction[1];
            if (shouldTurn(newRow, newCol, rows, cols, tag)) {
                dIndex = (dIndex + 1) % 4;
                direction = directions[dIndex];
                newRow = row + direction[0];
                newCol = col + direction[1];
            }
            row = newRow;
            col = newCol;
        }
        return list;
    }

    private boolean shouldTurn(int newRow, int newCol, int rows, int cols, boolean[][] tag) {
        return newRow < 0 || newRow >= rows || newCol < 0 || newCol >= cols || tag[newRow][newCol];
    }
}
