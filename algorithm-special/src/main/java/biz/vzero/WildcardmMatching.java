package biz.vzero;

/**
 * @author duzeng
 * @className WildcardmMatching.java
 * @description leetcode-44
 * @date 2023年10月23日 14:58
 */
public class WildcardmMatching {
    public boolean isMatch(String s, String p) {
        var m = s.length();
        var n = p.length();
        var dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;

        for (int i = 1; i <= n; i++) {
            if (p.charAt(i - 1) == '*') {
                dp[0][i] = true;
            } else {
                break;
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (p.charAt(j - 1) == '*') {
                    dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
                } else {
                    if (matches(s, p, i, j)) {
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                }
            }

        }
        return dp[m][n];

    }

    private boolean matches(String s, String p, int i, int j) {
        if (i == 0) return false;
        if (p.charAt(j - 1) == '?') return true;
        return s.charAt(i - 1) == p.charAt(j - 1);
    }
}
