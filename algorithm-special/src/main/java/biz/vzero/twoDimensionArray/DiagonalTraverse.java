package biz.vzero.twoDimensionArray;

import java.util.ArrayList;

/**
 * @author duzeng
 * @className DiagonalTraverse.java
 * @description TODO
 * @date 2023年10月22日 17:40
 */
public class DiagonalTraverse {
    public int[] findDiagonalOrder(int[][] mat) {
        var rows = mat.length;
        var cols = mat[0].length;
        var list = new int[rows * cols];
        int row = 0, col = 0;
        boolean forword = true;
        var index = 0;
        while (row < rows && col < cols) {
            list[index] = mat[row][col];
            index++;
            if (forword) {
                if ((row - 1) < 0 && col!=cols-1) {
                    col = col + 1;
                    forword = !forword;
                } else if ((col + 1) >= cols) {
                    row = row + 1;
                    forword = !forword;
                } else {
                    row = row - 1;
                    col = col + 1;
                }
            } else {
                if (col - 1 < 0 && row!=rows-1) {
                    row = row + 1;
                    forword = !forword;
                } else if (row + 1 >= rows) {
                    col = col + 1;
                    forword = !forword;

                } else {
                    row = row + 1;
                    col = col - 1;
                }
            }
        }

        return list;
    }
}
