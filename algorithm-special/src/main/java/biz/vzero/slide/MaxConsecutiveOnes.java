package biz.vzero.slide;

/**
 * @author duzeng
 * @className MaxConsecutiveOnes.java
 * @description TODO
 * @date 2023年10月23日 09:13
 */
public class MaxConsecutiveOnes {
    public int longestOnes(int[] nums, int k) {

        var n = nums.length;
        var result = 0;
        var left = 0;
        var right = 0;
        var zeros = 0;

        while (right < n) {
            if (nums[right] == 0) {
                zeros++;
            }

            while (zeros > k) {
                if (nums[left] == 0) {
                    zeros--;
                }
                left++;
            }
            result = Math.max(result, right - left + 1);
            right++;

        }
        return result;
    }
}
