package biz.vzero.slide;

import java.util.Arrays;

/**
 * @author duzeng
 * @className MaxPointsFromCards.java
 * @description leetcode-1423
 * @date 2023年10月23日 08:04
 */
public class MaxPointsFromCards {
    public int maxScore(int[] cardPoints, int k) {
        var n = cardPoints.length;
        var windowSize = n - k;
        var sum = 0;
        for (int i = 0; i < windowSize; i++) {
            sum += cardPoints[i];
        }
        var minSUm = sum;
        for (int i = windowSize; i < n; i++) {

            sum += cardPoints[i] - cardPoints[i - windowSize];
            minSUm = Math.min(minSUm, sum);
        }

        return Arrays.stream(cardPoints).sum() - minSUm;

    }


}
