package biz.vzero.slide;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author duzeng
 * @className LongestSubstrNoRepeat.java
 * @description leetcode-3
 * @date 2023年10月23日 08:41
 */
public class LongestSubstrNoRepeat {
    public int lengthOfLongestSubstring(String s) {

        Set<Character> sub = new HashSet<>();
        var right = -1;
        var longest = 0;
        for (int i = 0; i < s.length(); i++) {
            if (i != 0) {
                sub.remove(s.charAt(i - 1));
            }

            while (right + 1 < s.length() && !sub.contains(s.charAt(right + 1))) {
                sub.add(s.charAt(right + 1));
                right++;
            }

            longest = Math.max(longest, right - i + 1);

        }
        return longest;
    }
}
