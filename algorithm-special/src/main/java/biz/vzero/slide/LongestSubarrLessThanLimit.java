package biz.vzero.slide;

import java.util.TreeMap;

/**
 * @author duzeng
 * @className LongestSubarrLessThanLimit.java
 * @description leetcode-1438
 * @date 2023年10月23日 09:48
 */
public class LongestSubarrLessThanLimit {
    public int longestSubarray(int[] nums, int limit) {
        var map = new TreeMap<Integer, Integer>();
        var n = nums.length;
        var left = 0;
        var right = 0;
        var longest = 0;

        while (right < n) {
            map.put(nums[right], map.getOrDefault(nums[right], 0) + 1);
            while (map.lastKey() - map.firstKey() > limit) {
                map.put(nums[left], map.get(nums[left]) - 1);
                if (map.get(nums[left]) == 0) {
                    map.remove(nums[left]);
                }
                left++;
            }
            longest = Math.max(longest, right - left + 1);
            right++;
        }

        return longest;
    }
}
