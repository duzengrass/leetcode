package biz.vzero.sort;

/**
 * @author duzeng
 * @className QuickSort.java
 * @description TODO
 * @date 2023年10月18日 19:12
 */
public class QuickSort {
    public void sort(int[] nums) {
        sort(nums, 0, nums.length - 1);
    }

    private void sort(int[] nums, int start, int end) {
        if (start < end) {
            var q = partition(nums, start, end);
            sort(nums, start, q - 1);
            sort(nums, q + 1, end);
        }
    }

    private int partition(int[] nums, int start, int end) {
        var pivot = nums[end];
        var sIndex = start - 1;
        for (int i = start; i <= end - 1; i++) {
            if (nums[i] < pivot) {
                sIndex++;
                swap(nums, sIndex, i);
            }
        }
        swap(nums, sIndex + 1, end);
        return sIndex + 1;
    }

    private void swap(int[] nums, int x, int y) {
        var a = nums[x];
        nums[x] = nums[y];
        nums[y] = a;
    }
}
