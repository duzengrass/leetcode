package biz.vzero.backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * @author duzeng
 * @className Queen.java
 * @description leetcode--0051
 * @date 2023年10月17日 11:50
 */
public class NQueen {
    private static final char BLANK = '.';
    private static final char QUEEN = 'Q';

    public List<List<String>> solveNQueens(int n) {
        char[][] board = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = BLANK;
            }
        }
        List<List<String>> result = new ArrayList<>();
        backtrack(board, 0, result);
        return result;
    }

    private void backtrack(char[][] board, int row, List<List<String>> result) {
        if (row == board.length) {
            List<String> temp = new ArrayList<>();
            for (int i = 0; i < board.length; i++) {
                temp.add(String.valueOf(board[i]));
            }
            result.add(temp);
            return;
        }

        var cols = board[0].length;
        //当前行row,遍历每一列
        for (int col = 0; col < cols; col++) {
            if (valid(row, col, board)) {
                board[row][col] = QUEEN;
                backtrack(board, row + 1, result);
                board[row][col] = BLANK;
            }
        }
    }

    private boolean valid(int row, int col, char[][] board) {
        //当前列的上方有无Q
        for (int i = row - 1; i >= 0; i--) {
            if (board[i][col] == QUEEN) {
                return false;
            }
        }
        var cols = board[0].length;
        //当前行的右斜上方有无Q
        for (int i = row - 1, j = col + 1; i >= 0 && j < cols; i--, j++) {
            if (board[i][j] == QUEEN) return false;
        }
        //当前行的左斜上方有无Q
        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == QUEEN) return false;
        }
        return true;
    }


}
