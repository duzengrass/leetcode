package biz.vzero.backtracking;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author duzeng
 * @className BacktrackingPhoneNumber.java
 * @description leetcode-0017
 * @date 2023年10月16日 16:44
 */
public class BacktrackingPhoneNumber {
    private static final Map<Character, String> MAPS = new HashMap<>() {
        {
            put('2', "abc");
            put('3', "def");
            put('4', "ghi");
            put('5', "jkl");
            put('6', "mno");
            put('7', "pqrs");
            put('8', "tuv");
            put('9', "wxyz");
        }
    };

    /**
     * 广度优先搜索
     *
     * @param digits
     * @return
     */
    public List<String> letterCombinations(String digits) {
        if (digits.isEmpty()) return Collections.emptyList();
        Queue<String> queue = new LinkedList<>();

        queue.offer("");
        int index = 0;
        var list = new ArrayList<String>();
        while ((!queue.isEmpty())) {
            var current = queue.poll();
            if (current.length() == digits.length()) {
                list.add(current);
                continue;
            }
            if (current.length() > index) {
                index++;
            }

            var letters = MAPS.get(digits.charAt(index));
            for (int i = 0; i < letters.length(); i++) {
                queue.offer(current + letters.charAt(i));
            }
        }
        return list;
    }

    /**
     * 深度优先搜索
     *
     * @param digits
     * @return
     */
    public List<String> letterCombinations2(String digits) {
        if (digits.isEmpty()) return Collections.emptyList();

        var result = new ArrayList<String>();

        track(digits, 0, "", result);
        return result;
    }

    public void track(String digits, int index, String current, List<String> result) {
        if (index == digits.length()) {
            result.add(current);
            return;
        }
        var letters = MAPS.get(digits.charAt(index));
        for (int i = 0; i < letters.length(); i++) {
            track(digits, index + 1, current + letters.charAt(i), result);
        }
    }

}
