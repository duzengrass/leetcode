package biz.vzero;

/**
 * @author duzeng
 * @className MedianTwoSortedArray.java
 * @description TODO
 * @date 2023年10月23日 00:11
 */
public class MedianTwoSortedArray {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        var nums = new int[nums1.length + nums2.length];
        var index1 = 0;
        var index2 = 0;
        while (index1 < nums1.length || index2 < nums2.length) {

            if (index1 >= nums1.length) {
                nums[index1 + index2] = nums2[index2];
                index2++;
                continue;
            } else if (index2 >= nums2.length) {
                nums[index1 + index2] = nums1[index1];
                index1++;
                continue;
            }

            if (nums1[index1] > nums2[index2]) {
                nums[index1 + index2] = nums2[index2];
                index2++;
            } else if (nums1[index1] < nums2[index2]) {
                nums[index1 + index2] = nums1[index1];
                index1++;
            } else {
                nums[index1 + index2] = nums1[index1];
                nums[index1 + index2 + 1] = nums1[index1];
                index1++;
                index2++;
            }
        }
        if (nums.length % 2 == 1) {
            return nums[nums.length / 2];
        } else {
            return (nums[nums.length / 2] + nums[nums.length / 2 - 1]) / 2.0;
        }
    }
}
