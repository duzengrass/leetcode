package biz.vzero;

/**
 * @author duzeng
 * @className BinarySearch.java
 * @description TODO
 * @date 2023年10月10日 20:52
 */
public class BinarySearch {
    public int search(int[] nums, int target) {
        return search(nums, target, 0, nums.length - 1);
    }

    public int search(int[] nums, int target, int lo, int hi) {
        if (lo > hi) return -1;
        var mid = lo + (hi - lo) / 2;
        if (nums[mid] > target) {
            return search(nums, target, lo, mid - 1);
        } else if (nums[mid] < target) {
            return search(nums, target, mid + 1, hi);
        } else {
            return mid;
        }
    }
}
