package biz.vzero;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className MySqrtSolutionTest.java
 * @description TODO
 * @date 2022年08月21日 19:11
 */
class MySqrtSolutionTest {

    private static Stream<Arguments> provideData() {
        return Stream.of(
                Arguments.of(4, 2),
                Arguments.of(8, 2)
        );
    }

    @ParameterizedTest
    @MethodSource("provideData")
    void mySqrt(int input, int output) {
        assertEquals(output, new MySqrtSolution().mySqrt(input));
    }
}