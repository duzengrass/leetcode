package biz.vzero;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className QuickSortTest.java
 * @description TODO
 * @date 2023年04月18日 17:02
 */
class QuickSortTest {

    private QuickSort quickSort;

    @BeforeEach
    void setUp() {
        quickSort = new QuickSort();
    }

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of(new int[]{5, 7, 9, 12, 1, 4}, new int[]{1, 4, 5, 7, 9, 12}),
                Arguments.of(new int[]{11, 5, 9, 3, 7}, new int[]{3, 5, 7, 9, 11}),
                Arguments.of(new int[]{43, 12, 99, 12, 123, 1, 6, 74, 54, 33, 12, 13}, new int[]{1, 6, 12, 12, 12, 13, 33, 43, 54, 74, 99, 123})
        );
    }

    @ParameterizedTest
    @MethodSource("provide")
    void sort(int[] org, int[] target) {
        quickSort.sort(org);
        assertArrayEquals(target, org);
    }
}