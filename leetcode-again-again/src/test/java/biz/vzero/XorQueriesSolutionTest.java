/**
 * 
 */
package biz.vzero;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author duzeng
 *
 */
class XorQueriesSolutionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	public static Stream<Arguments> provideData() {

		return Stream.of(
				Arguments.of(new int[] { 1, 3, 4, 8 },
						new int[][] { new int[] { 0, 1 }, new int[] { 1, 2 }, new int[] { 0, 3 }, new int[] { 3, 3 } },
						new int[] { 2, 7, 14, 8 }),

				Arguments.of(new int[] { 4, 8, 2, 10 },
						new int[][] { new int[] { 2, 3 }, new int[] { 1, 3 }, new int[] { 0, 0 }, new int[] { 0, 3 } },
						new int[] { 8, 0, 4, 4 })

		);
	}

	@ParameterizedTest
	@MethodSource("provideData")
	void test(int[] arr, int[][] queries, int[] expected) {
		var actual = new XorQueriesSolution().xorQueries(arr, queries);
		assertNotNull(actual);

		assertArrayEquals(expected, actual);
	}	
	
	@ParameterizedTest
	@MethodSource("provideData")
	void testPrefixXor(int[] arr, int[][] queries, int[] expected) {
		var actual = new XorQueriesSolution().prefixXor(arr, queries);
		assertNotNull(actual);

		assertArrayEquals(expected, actual);
	}

}
