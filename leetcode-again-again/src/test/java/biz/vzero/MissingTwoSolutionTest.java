package biz.vzero;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className MissingTwoSolutionTest.java
 * @description TODO
 * @date 2022年08月21日 18:57
 */
class MissingTwoSolutionTest {

    private static Stream<Arguments> provideData() {
        return Stream.of(
                Arguments.of(new int[]{1}, new int[]{2, 3}),
                Arguments.of(new int[]{2, 3}, new int[]{1, 4})
        );
    }

    @ParameterizedTest
    @MethodSource("provideData")
    void missingTwo(int[] input, int[] output) {
        var solution = new MissingTwoSolution();
        var result = solution.missingTwo(input);
        assertArrayEquals(output, result);
    }
}