package biz.vzero;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;

/**
 * @author duzeng
 * @className JmmTst.java
 * @description TODO
 * @date 2023年06月19日 20:43
 */
public class JmmTest {

    private int A = 0;
    private int B = 0;

    @Test
    void abc() throws InterruptedException {
        var abc = Executors.newFixedThreadPool(2);
        abc.submit(() -> {
            int r2 = A;
            B = 2;
            System.out.println("r2: " + r2);
        });
        abc.submit(() -> {
            int r1 = B;
            A = 1;
            System.out.println("r1: " + r1);
        });
    }
}
