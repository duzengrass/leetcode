package biz.vzero;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author duzeng
 * @className FindRightIntervalSolutionTest.java
 * @description TODO
 * @date 2022年08月21日 19:39
 */
class FindRightIntervalSolutionTest {

    private static Stream<Arguments> provideData() {
        return Stream.of(
                Arguments.of(new int[][]{{1, 2}}, new int[]{-1}),
                Arguments.of(new int[][]{{3, 4}, {2, 3}, {1, 2}}, new int[]{-1, 0, 1}),
                Arguments.of(new int[][]{{1, 4}, {2, 3}, {3, 4}}, new int[]{-1, 2, -1})
        );
    }

    @ParameterizedTest
    @MethodSource("provideData")
    void find(int[][] input, int[] output) {
        assertArrayEquals(output, new FindRightIntervalSolution().find(input));
    }
}