package biz.vzero;

/**
 * @author duzeng
 * @className MySqrtSolution.java
 * @description TODO
 * @date 2022年08月21日 19:11
 */
public class MySqrtSolution {
    public int mySqrt(int x) {
        int left = 0, right = x, ans = -1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if ((long)mid * mid <= x) {
                ans = mid;
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return ans;
    }
}
