package biz.vzero;

/**
 * @author duzeng
 * @className QuickSort.java
 * @description TODO
 * @date 2023年04月18日 16:55
 */
public class QuickSort {
    public void sort(int[] arr) {
        this.sort(arr, 0, arr.length - 1);
    }

    public void sort(int[] arr, int begin, int end) {
        if (begin < end) {
            var partitionIndex = partition(arr, begin, end);
            this.sort(arr, begin, partitionIndex - 1);
            this.sort(arr, partitionIndex + 1, end);
        }
    }

    private int partition(int[] arr, int begin, int end) {

        var pivot = arr[end];
        var index = begin - 1;

        for (int i = begin; i < end; i++) {
            if (arr[i] <= pivot) {
                index++;
                var swap = arr[index];
                arr[index] = arr[i];
                arr[i] = swap;
            }
        }

        var swap = arr[index + 1];
        arr[index + 1] = arr[end];
        arr[end] = swap;
        return index + 1;
    }
}
