package biz.vzero;

import java.util.Iterator;

public class XorQueriesSolution {

	public int[] xorQueries(int[] arr, int[][] queries) {

		var result = new int[queries.length];

		for (int i = 0; i < queries.length; i++) {
			var query = queries[i];
			var start = query[0];
			var end = query[1];

			var temp = arr[start];

			for (int j = start + 1; j <= end; j++) {
				temp = temp ^ arr[j];
			}
			result[i] = temp;
		}
		return result;
	}

	/**
	 * 前缀异或数组结果缓存
	 * @see <a herf="https://leetcode.cn/problems/xor-queries-of-a-subarray/solution/zi-shu-zu-yi-huo-cha-xun-by-leetcode-solution/">leetcode</a>
	 * @param arr
	 * @param queries
	 * @return
	 */
	public int[] prefixXor(int[] arr, int[][] queries) {
		int[] xors = new int[arr.length + 1];

		for (int i = 0; i < arr.length; i++) {
			xors[i + 1] = xors[i] ^ arr[i];
		}

		int[] ans = new int[queries.length];

		for (int i = 0; i < queries.length; i++) {
			ans[i] = xors[queries[i][0]] ^ xors[queries[i][1] + 1];
		}

		return ans;

	}
}
