package biz.vzero;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author duzeng
 * @className CompletableFutureDemo.java
 * @description TODO
 * @date 2022年08月07日 22:52
 */
public class CompletableFutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Integer age = 1;

        CompletableFuture<Void> maturityFuture = CompletableFuture.supplyAsync(() -> {
            if (age < 0) {
                throw new IllegalArgumentException("Age can not be negative");
            }
            if (age > 18) {
                return "Adult";
            } else {
                return "Child";
            }
        }).exceptionally(ex -> {
            System.out.println("Oops! We have an exception - " + ex.getMessage());
            return "Unknown!";
        }).thenAccept(result -> {
            System.out.println(result);
        });

//        System.out.println("Maturity : " + maturityFuture.get());
        maturityFuture.get();
    }
}
